package jtm.extra14;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class CoinFlipTest {

    @Test
    public void oneTest(){
        assertArrayEquals(new String[]{"H", "T"},
                CoinFlip.coinFlip(1));
    }

    @Test
    public void twoTest(){
        assertArrayEquals(new String[]{"HH", "HT", "TH", "TT"},
                CoinFlip.coinFlip(2));
    }

    @Test
    public void threeTest(){
        assertArrayEquals(new String[]{"HHH", "HHT", "HTH", "HTT", "THH", "THT", "TTH", "TTT"},
                CoinFlip.coinFlip(3));
    }
}
