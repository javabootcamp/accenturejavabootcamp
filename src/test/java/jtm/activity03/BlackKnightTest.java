package jtm.activity03;

import static jtm.TestUtils.handleErrorAndFail;
import static jtm.activity03.BlackKnight.knights;
import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;

public class BlackKnightTest {
	private static Logger logger = Logger.getLogger(BlackKnightTest.class);

	static int startNo, leftNo;
	static String[] names = { "Arthur", "Cnut", "Edward", "Edmund", "Harold", "Henry", "William" };
	static BlackKnight knight;

	public static void setBattle(int startNo) {

		leftNo = startNo;
		BlackKnight.setBattle(startNo);
		for (int i = 0; i < startNo; i++) {
			knight = new BlackKnight(names[i]);
			logger.debug("Knight " + knight.name + " was created");
		}
	}

	@Test
	public void testKnightResponses() {
		try {
			startNo = 3;
			setBattle(startNo);
			assertEquals("Total number of knights is wrong.", startNo, BlackKnight.totalKnights);
			assertEquals("Total number of alive knights is wrong.", startNo, BlackKnight.aliveKnights);
			assertEquals("Wrong response for cut off arm.", "Bugger!", knights[0].cutOffArm());
			assertEquals("Wrong response for cut off arm.", "Bugger!", knights[0].cutOffArm());
			assertEquals("Wrong response for cut off arm.", "Haah!", knights[0].cutOffArm());
			assertEquals("Wrong response for cut off arm.", "Haah!", knights[0].cutOffArm());
			assertEquals("Wrong response for cut off leg.", "Bollocks!", knights[0].cutOffLeg());
			assertEquals("Wrong response for cut off leg.", "Bollocks!", knights[0].cutOffLeg());
			assertEquals("Wrong response for cut off leg.", "Haah!", knights[0].cutOffLeg());
			assertEquals("Wrong response for cut off leg.", "Haah!", knights[0].cutOffLeg());
			assertEquals("Wrong response for cut off leg.", "Haah!", knights[0].cutOffLeg());
			assertEquals("Wrong response for cut off head.", "You'l newer win! Cnut, Edward will still fight!",
					knights[0].cutOffHead());
			assertEquals("Wrong response for cut off head.", "You'l newer win! Edward will still fight!",
					knights[1].cutOffHead());
			assertEquals("Wrong response for cut off head.", "You'l burn in hell forever!", knights[2].cutOffHead());
			assertEquals("Wrong response for cut off head.", "Only chicken beats dead!", knights[2].cutOffHead());
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

	@Test
	public void testStaticCounters() {
		try {
			startNo = (int) (Math.random() * 6 + 1);
			setBattle(startNo);
			assertEquals("Wrong total amount of knights.", startNo, BlackKnight.totalKnights);
			assertEquals("Wrong amount of alive knights.", startNo, BlackKnight.aliveKnights);
			BlackKnight.knights[0].cutOffHead();
			leftNo--;
			assertEquals("Wrong amount of alive knights.", leftNo, BlackKnight.aliveKnights);
			if (leftNo > 0) {
				for (int i = 1; i <= leftNo; i++) {
					BlackKnight.knights[i].cutOffHead();
					assertEquals("Wrong amount of alive knights.", leftNo - i, BlackKnight.aliveKnights);
				}
			}
			assertEquals("Wrong amount of alive knights.", 0, BlackKnight.aliveKnights);
			assertEquals("Wrong total amount of knights.", startNo, BlackKnight.totalKnights);
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

}
