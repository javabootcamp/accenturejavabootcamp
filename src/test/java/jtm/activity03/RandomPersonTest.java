
package jtm.activity03;

import static jtm.TestUtils.handleErrorAndFail;
import static jtm.TestUtils.invokeGetMethod;
import static jtm.TestUtils.invokeSetMethod;
import static jtm.TestUtils.randomWord;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Random;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

public class RandomPersonTest {

	private static Logger logger = Logger.getLogger(RandomPersonTest.class);
	private static RandomPerson vars;
	private static Object result;

	private static String name = "";
	private static Integer age;
	private static Float weight;
	private static Boolean isFemale;
	private static Character smile;
	private static Random rnd;
	private static char[] smileys = { '☺', '☹', '☻' };

	@BeforeClass
	public static void setUp() {
		vars = new RandomPerson();
		rnd = new Random();
		name = randomWord();
		age = rnd.nextInt(50) + 1;
		weight = rnd.nextFloat() * 100f + 2.5f;
		isFemale = rnd.nextBoolean();
		smile = smileys[rnd.nextInt(3)];
	}

	@Test
	public void testFields() {
		TestUtils tu = new TestUtils();
		try {
			logger.debug(tu.listFields(vars));
			fail("Random person fields are not private.");
		} catch (Exception e) {
			assertEquals("Random person fields are not private.",e.getClass(), IllegalAccessException.class);
		}
	}

	@Test
	public void testMethods() {

		try {
			// Age
			result = invokeGetMethod(vars, "jtm.activity03.RandomPerson", "getAge");
			assertEquals("Wrong default age.", 0, result);
			invokeSetMethod(vars, "jtm.activity03.RandomPerson", "setAge", age);
			result = invokeGetMethod(vars, "jtm.activity03.RandomPerson", "getAge");
			assertEquals("Wrong retrieved age.", age, result);

			// Name
			result = invokeGetMethod(vars, "jtm.activity03.RandomPerson", "getName");
			assertEquals("Wrong default name", null, result);
			invokeSetMethod(vars, "jtm.activity03.RandomPerson", "setName", name);
			result = invokeGetMethod(vars, "jtm.activity03.RandomPerson", "getName");
			assertEquals("Wrong retrieved name", name, result);

			// isFemale
			result = invokeGetMethod(vars, "jtm.activity03.RandomPerson", "isFemale");
			assertEquals("Wrong default gender.", false, result);
			invokeSetMethod(vars, "jtm.activity03.RandomPerson", "setFemale", isFemale);
			result = invokeGetMethod(vars, "jtm.activity03.RandomPerson", "isFemale");
			assertEquals("Wrong retrieved gender.", isFemale, result);

			// smile
			result = invokeGetMethod(vars, "jtm.activity03.RandomPerson", "getSmile");
			assertEquals("Wrong default smile.", '\0', result);
			invokeSetMethod(vars, "jtm.activity03.RandomPerson", "setSmile", smile);
			result = invokeGetMethod(vars, "jtm.activity03.RandomPerson", "getSmile");
			assertEquals("Wrong retrieved smile.", smile, result);

			// weight
			result = invokeGetMethod(vars, "jtm.activity03.RandomPerson", "getWeight");
			assertEquals("Wrong default weight", 0f, result);
			invokeSetMethod(vars, "jtm.activity03.RandomPerson", "setWeight", weight);
			result = invokeGetMethod(vars, "jtm.activity03.RandomPerson", "getWeight");
			assertEquals("Wrong retrieved weight.", weight, (Float) result, 0.1f);
			logger.info("OK " + name + " " + smile);

		} catch (Exception e) {
			handleErrorAndFail(e);
		}

	}

}
