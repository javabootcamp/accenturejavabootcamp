
package jtm.activity03;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Random;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

public class ArrayTest {
	private static Logger logger = Logger.getLogger(ArrayTest.class);
	private int[] should, is;
	private String[] args;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Random rnd = new Random();
		int size = rnd.nextInt(20) + 1;
		should = new int[size];
		args = new String[size];
		for (int i = 0; i < should.length; i++) {
			should[i] = rnd.nextInt(10);
			args[i] = Integer.toString(should[i]);
		}
		logger.debug("Start: " + Arrays.toString(should));
	}

	@Test
	public void testMain() {
		try {

			boolean swapped = true;
			int n = 0;
			int tmp;
			while (swapped) {
				swapped = false;
				n++;
				for (int i = 0; i < should.length - n; i++) {
					if (should[i] > should[i + 1]) {
						tmp = should[i];
						should[i] = should[i + 1];
						should[i + 1] = tmp;
						swapped = true;
					}
				}
			}

			logger.debug("Should be: " + Arrays.toString(should));
			Array.main(args);

			is = Array.returnSortedArray();
			System.out.print("Sorted array:");
			Array.printSortedArray();

			logger.debug("Actual is: " + Arrays.toString(is));

			assertTrue("Array seems incorrectly sorted.", Arrays.equals(should, is));
			logger.info("OK");

		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}
}
