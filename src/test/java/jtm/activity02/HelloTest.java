package jtm.activity02;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;

import static jtm.TestUtils.stripLineBreaks;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HelloTest {

	private static Logger logger = Logger.getLogger(HelloTest.class);
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	PrintStream prevOut = null;

	@BeforeClass
	public static void beforeClass() {
		TestUtils.checkOS();
	}

	@Test
	public void testMain() {

		try {
			// Keep current System.out with us
			prevOut = System.out;
			// Set System.out to test stream
			System.setOut(new PrintStream(outContent));
			Class<?> cls = Class.forName("jtm.activity02.Hello");
			Method meth = cls.getMethod("main", String[].class);
			String[] params = null;
			meth.invoke(null, (Object) params);
			// Restore previous System.out
			System.setOut(prevOut);
			assertEquals("Method should print: Hello world!", "Hello world!", stripLineBreaks(outContent.toString()));
			logger.info("Hello class OK");
		} catch (Exception e) {
			fail("main method of Hello class should print: 'Hello world!'");
		} finally {
			// Restore previous System.out
			System.setOut(prevOut);
		}

	}

}
