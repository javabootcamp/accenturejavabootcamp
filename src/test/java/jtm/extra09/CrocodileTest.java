package jtm.extra09;

import static jtm.extra09.BoardTest.reference;
import static jtm.extra09.BoardTest.setCandies;
import static org.junit.Assert.assertEquals;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CrocodileTest {

	private static Logger logger = Logger.getLogger(CrocodileTest.class);
	static Crocodile crocodile;
	static int x, y, moves, candies;
	static char[][] tmpArr;
	static Board board;

	@Before
	public void before() {
		BoardTest.setUp();
		setCandies(reference);
		tmpArr = reference.getArrayClone();
	}

	@Test
	public void test1CrocodileSimple() {
		try {
			board = new Board(tmpArr);
			logger.debug("board before movement: \n" + board);
			crocodile = new CrocodileSimple();
			reference.moveSimple();
			crocodile.move(board);
			logger.debug("board after movement:\n" + board);
			logger.debug(crocodile);
			assertEquals("Wrong count of moves.", reference.crocMoves, crocodile.getMoves());
			assertEquals("Wrong count of eaten candies", reference.crocCandies, crocodile.getCandies());
			assertEquals("Wrong board state after crocodile movement", reference.toString(), board.toString());
			assertEquals("Wrong number of remaining candies on board", reference.countArrayCandies(),
					board.countBoardCandies());
			assertEquals("Crocodyle type error", "CrocodileSimple", crocodile.getType());
			logger.info("OK");
		} catch (Exception e) {
			TestUtils.handleErrorAndFail(e);
		}
	}

	@Test
	public void test2CrocodileGreedey() {
		try {
			board = new Board(tmpArr);
			logger.debug("board before movement: \n" + board);
			crocodile = new CrocodileGreedy();
			reference.moveGreedy();
			crocodile.move(board);
			logger.debug("board after movement:\n" + board);
			logger.debug(crocodile);
			assertEquals("Wrong count of moves.", reference.crocMoves, crocodile.getMoves());
			assertEquals("Wrong count of eaten candies", reference.crocCandies, crocodile.getCandies());
			assertEquals("Wrong board state after crocodile movement", reference.toString(), board.toString());
			assertEquals("Wrong number of remaining candies on board", reference.countArrayCandies(),
					board.countBoardCandies());
			assertEquals("Crocodyle type error", "CrocodileGreedy", crocodile.getType());
			logger.info("OK");
		} catch (Exception e) {
			TestUtils.handleErrorAndFail(e);
		}
	}

}
