package jtm.extra09;

import static jtm.extra09.BoardTest.reference;
import static jtm.extra09.BoardTest.setCandies;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

public class GameTest {

	private static Logger logger = Logger.getLogger(GameTest.class);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		try {
			CrocodileGame.runGame();
			fail("Lazy initialization error. Can call runGame() before game construction.");
		} catch (NullPointerException e) {
			logger.debug("Lazy initialization test for runGame() call OK");
		}
		try {
			assertNull("Lazy initialization error. Board is initialized before game construction.",
					CrocodileGame.board);
			assertNull("Lazy initialization error. Crocodile is initialized before game construction.",
					CrocodileGame.crocodile);
			BoardTest.setUp();
			setCandies(reference);
		} catch (Exception e) {
			TestUtils.handleErrorAndFail(e);
		}
	}

	@Test
	public void test() {
		try {
			GameFactory.setBoard(new Board(reference.getArrayClone()));
			GameFactory.addCrocodile("CrocodileSimple");
			GameFactory.addCrocodile("CrocodileGreedy");
			logger.debug("Board:\n" + CrocodileGame.board);
			String suspected = BoardTest.reference.runGame();
			logger.debug("Suspected game result: " + suspected);
			String actual = CrocodileGame.runGame();
			assertEquals("Wrong game result", suspected, actual);
			logger.info("Game result: " + actual);
			logger.info("OK");
		} catch (Exception e) {
			TestUtils.handleErrorAndFail(e);
		}
	}
}
