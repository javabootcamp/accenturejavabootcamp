package jtm.extra09;

import org.apache.log4j.Logger;

public class Reference {
	char[][] array;
	int crocMoves;
	int crocCandies;

	private static Logger logger = Logger.getLogger(Reference.class);

	public Reference(int x, int y) {
		crocMoves = 0;
		crocCandies = 0;
		array = new char[y][x];
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[0].length; j++) {
				array[i][j] = '○';
			}
		}
		logger.debug("Board size: " + getX() + "×" + getY());
	}

	public Reference(char[][] array) {
		crocMoves = 0;
		crocCandies = 0;
		this.array = array;
		logger.debug("Board size: " + getX() + "×" + getY());
	}

	public void setCandy(int x, int y, char candy) {
		array[y][x] = candy;
	}

	public char getCandy(int x, int y) {
		return array[y][x];
	}

	public int getX() {
		return array[0].length;
	}

	public int getY() {
		return array.length;
	}

	public int countArrayCandies() {
		int candies = 0;
		for (int j = 0; j < getY(); j++) {
			for (int i = 0; i < getX(); i++) {
				if (getCandy(i, j) == '●')
					candies++;
			}
		}
		return candies;
	}

	public void moveSimple() {
		int x = 0, y = 0, i = 0, j = 0;
		crocCandies = 0;
		crocMoves = 0;
		x = getX();
		y = getY();

		for (i = 0; i < x; i++) {
			if (getCandy(i, j) == '●')
				crocCandies++;
			setCandy(i, j, '◎');
		}
		// i == x when first for loop exits, therefore
		i--;
		logger.trace("x:" + x + " i:" + i);
		for (j = 0; j < y; j++) {
			if (getCandy(i, j) == '●')
				crocCandies++;
			setCandy(i, j, '◎');
		}
		crocMoves = x + y - 2;
	}

	public void moveGreedy() {
		int x = 0, y = 0, i = 0, j = 0;
		crocCandies = 0;
		crocMoves = 0;
		x = getX();
		y = getY();
		for (i = 0; i < x; i++) {
			for (j = 0; j < y; j++) {
				if (getCandy(i, j) == '●')
					crocCandies++;
				setCandy(i, j, '◎');
			}
		}
		// If even number of rows, need to walk last row twice
		if (y % 2 == 0)
			crocMoves = x * y - 1 + x - 1;
		// else just walk all rows
		else
			crocMoves = x * y - 1;
	}

	public String runGame() {
		String result = "";
		char[][] tmp = getArrayClone();
		moveSimple();
		float scoreSimple = (float) crocCandies / (float) crocMoves;
		logger.debug("CrocodileSimple " + crocCandies + "/" + crocMoves + "=" + scoreSimple);
		array = tmp;
		moveGreedy();
		float scoreGreedy = (float) crocCandies / (float) crocMoves;
		logger.debug("CrocodileGreedy " + crocCandies + "/" + crocMoves + "=" + scoreGreedy);
		if (Float.toString(scoreSimple).equals("Infinity") && Float.toString(scoreGreedy).equals("Infinity")
				|| Float.toString(scoreSimple).equals("NaN") && Float.toString(scoreGreedy).equals("NaN")
				|| Math.abs(scoreSimple - scoreGreedy) < 0.001)
			result = "Tie";
		else if (scoreSimple > scoreGreedy)
			result = "CrocodileSimple wins";
		else
			result = "CrocodileGreedy wins";
		return result;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int j = 0; j < getY(); j++) {
			for (int i = 0; i < getX(); i++) {
				sb.append(getCandy(i, j));
			}
			if (j < getY() - 1)
				sb.append('\n');
		}
		return sb.toString();
	}

	public char[][] getArrayClone() {
		char[][] tmp = new char[getY()][getX()];
		for (int j = 0; j < getY(); j++)
			for (int i = 0; i < getX(); i++)
				tmp[j][i] = array[j][i];
		return tmp;
	}

	public void setArray(char[][] array) {
		this.array = array;
	}

}
