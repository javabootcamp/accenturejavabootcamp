package jtm.extra09;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BoardTest {
	public static int boardX, boardY, step, range;
	public static Reference reference;
	static Board board;
	private static Logger logger = Logger.getLogger(BoardTest.class);

	@BeforeClass
	public static void setUp() {
		boardX = TestUtils.randomInt(5, 1);
		boardY = TestUtils.randomInt(3, 1);
		range = TestUtils.randomInt(3, 1); // average distance between candies
		step = TestUtils.randomInt(1);
		reference = new Reference(boardX, boardY);
	}

	@Test
	public void test0Constructor() {
		try {
			setCandies(reference);
			board = new Board(reference.array);
			logger.debug("Board size:" + boardX + "×" + boardY);
			assertEquals("Wrongly constructed board from array", reference.toString(), board.toString());
		} catch (Exception e) {
			TestUtils.handleErrorAndFail(e);
		}
	}

	@Test
	public void test1GetX() {
		try {
			assertEquals("Wrong board width.", boardX, board.getX());
		} catch (Exception e) {
			TestUtils.handleErrorAndFail(e);
		}
	}

	@Test
	public void test2GetY() {
		try {
			assertEquals("Wrong board height.", boardY, board.getY());
		} catch (Exception e) {
			TestUtils.handleErrorAndFail(e);
		}
	}

	@Test
	public void test3GetSetCandy() {
		try {
			setCandies(reference);
			board = new Board(reference.array);
			logger.debug("Candies:\n" + reference.toString());
			assertEquals("Board testing error.", reference.toString(), board.toString());
		} catch (Exception e) {
			TestUtils.handleErrorAndFail(e);
		}
	}

	@Test
	public void test4countCandies() {
		try {
			assertEquals("Wrong number of counted candies.", reference.countArrayCandies(), board.countBoardCandies());
		} catch (Exception e) {
			TestUtils.handleErrorAndFail(e);
		}
	}

	@Test
	public void test4getBoardClone() {
		try {
			Board tmp = board.getClone();
			assertNotEquals("Board is not cloned", tmp, board);
			assertEquals("Cloned board is different", tmp.toString(), board.toString());
		} catch (Exception e) {
			TestUtils.handleErrorAndFail(e);
		}
	}

	public static void setCandies(Reference reference) {
		int count = TestUtils.randomInt(1);
		int step = TestUtils.randomInt(1);
		int i = 0, j = 0;
		// Reset all fields to empty
		for (i = 0; i < reference.array.length; i++) {
			for (j = 0; j < reference.array[0].length; j++) {
				reference.array[i][j] = '○';
			}
		}
		// Randomly set candies
		while (count < reference.getX() * reference.getY()) {
			int setIt = TestUtils.randomInt(1);
			i = count % reference.getX();
			j = count / reference.getX();
			if (setIt == 0)
				reference.setCandy(i, j, '●');
			count = step * range;
			step++;
		}
		logger.trace("\nrefBoard:\n" + reference + "\nboard:\n" + board);
	}

}
