package jtm;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jacoco.core.data.ExecutionData;
import org.jacoco.core.data.ExecutionDataReader;
import org.jacoco.core.data.IExecutionDataVisitor;
import org.jacoco.core.data.ISessionInfoVisitor;
import org.jacoco.core.data.SessionInfo;

public class TestJacoco {

	public static void main(String[] args) {
		try {
			Map<String, List<Integer>> results = getExecutionResults("jtm.activity14.DatabaseUnitTest",
					"jtm/activity14/Student|jtm/activity14/StudentManager");
			System.out.println(results);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static Map<String, List<Integer>> getExecutionResults(final String className, final String pattern)
			throws IOException {
		// Delete previous statistics file, if it was not deleted already (e.g.
		// test failed)

		final Map<String, List<Integer>> result;
		result = new HashMap<String, List<Integer>>();
		// Load statistics from produced file
		final FileInputStream in = new FileInputStream("target/coverage-reports/jacoco-unit.exec");
		final ExecutionDataReader reader = new ExecutionDataReader(in);
		reader.setSessionInfoVisitor(new ISessionInfoVisitor() {
			@Override
			public void visitSessionInfo(final SessionInfo info) {
			}
		});
		reader.setExecutionDataVisitor(new IExecutionDataVisitor() {
			@Override
			public void visitClassExecution(final ExecutionData data) {
				String cpath = data.getName();
				// If classpath matches pattern, add coveraga results to the map
				if (cpath.matches(pattern)) {
					int probes = data.getProbes().length;
					int hits = getHitCount(data.getProbes());
					ArrayList<Integer> values = new ArrayList<Integer>();
					values.add(hits);
					values.add(probes);

					result.put(cpath, values);
				}
			}
		});
		reader.read();
		in.close();
		return result;
	}

	private static int getHitCount(final boolean[] data) {
		int count = 0;
		for (final boolean hit : data) {
			if (hit) {
				count++;
			}
		}
		return count;
	}

}
