package jtm.extra10;

import static jtm.TestUtils.handleErrorAndFail;
import static jtm.TestUtils.toParagraph;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import jtm.CommandResult;
import jtm.TestUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LogTest {
	static String now, today;

	// Parts of searched patterns in log file
	String prefix; // start part (date)
	String infix; // before classpath
	String suffix; // end part (message)
	String severity;
	String expected;

	CommandResult result; // Returned result

	@BeforeClass
	public static void beforeClass() {
		TestUtils.checkOS();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		now = df.format(new Date());
		today = now.substring(0, 10);
	}


	@Test
	public void test03CleanLog() {
		try {
			// Clear log file
			TestUtils.clearLogFile(Log.getLoger());
		} catch (Throwable e) {
			Assert.fail("Could not clear log file. Check that file logger is configured.");
		}

	}

	@Test
	public void test04Methods() {
		try {

			String[] list = { "public static org.apache.log4j.Logger jtm.extra10.Log.getLoger()",
					"public static void jtm.extra10.Log.debug(java.lang.String)",
					"public static void jtm.extra10.Log.error(java.lang.String)",
					"public static void jtm.extra10.Log.fatal(java.lang.String)",
					"public static void jtm.extra10.Log.info(java.lang.String)",
					"public static void jtm.extra10.Log.trace(java.lang.String)",
					"public static void jtm.extra10.Log.warn(java.lang.String)" };

			// Check, that all methods are implemented for tested classes
			assertEquals("Not all necessary methods are implemented for Log. ", toParagraph(list),
					TestUtils.checkMethods(list, "jtm.extra10.Log"));

			System.err.println(TestUtils.listMethods("jtm.extra10.Log$ExtLog"));

			String[] list1 = { "public static org.apache.log4j.Logger jtm.extra10.Log.getLoger()",
					"public static void jtm.extra10.Log$ExtLog.debug(java.lang.String)",
					"public static void jtm.extra10.Log$ExtLog.error(java.lang.String)",
					"public static void jtm.extra10.Log$ExtLog.fatal(java.lang.String)",
					"public static void jtm.extra10.Log$ExtLog.info(java.lang.String)",
					"public static void jtm.extra10.Log$ExtLog.trace(java.lang.String)",
					"public static void jtm.extra10.Log$ExtLog.warn(java.lang.String)"

			};

			// Check, that all methods are implemented for tested classes
			assertEquals("Not all necessary methods are implemented for Log. ", toParagraph(list1),
					TestUtils.checkMethods(list1, "jtm.extra10.Log$ExtLog"));

		} catch (Exception t) {
			handleErrorAndFail(t, "Could not get list of methods for Log");
		}
	}

	private void setExpected(String severity) {
		this.severity = severity;
		expected = now + " " + severity;
	}


}
