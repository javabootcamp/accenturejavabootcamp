package jtm.extra15;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static jtm.extra15.MorseCode.translateToMorse;
import static org.junit.Assert.assertEquals;

public class MorseCodeTest {


    @Test
    public void sampleTest(){
        String expected = "- .... .- -. -.- | -.-- --- ..- | ...- . .-. -.-- | -- ..- -.-. ....";
        String result = translateToMorse("Thank you very much");
        assertEquals(expected,result);
    }

    @Test
    public void emptyTest(){
        String expected = "";
        String result = translateToMorse("");
        assertEquals(expected,result);
    }

    @Test
    public void sgtTest(){
        String expected = "... .... . | --. --- . ... | - . -.-. ....";
        String result = translateToMorse("She Goes Tech");
        assertEquals(expected,result);
    }


}
