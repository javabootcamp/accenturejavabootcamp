package jtm.activity05;

import static jtm.TestUtils.handleErrorAndFail;

import jtm.TestUtils;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EncapsulationClassesTest {
	private static Logger logger = Logger.getLogger(EncapsulationClassesTest.class);

	@Test
	public void test01TestClasses() {
		try {
			Class.forName("jtm.activity05.Vehicle");
			Class.forName("jtm.activity05.Ship");
			Class.forName("jtm.activity05.Amphibia");
			Class.forName("jtm.activity05.WaterRoad");
		} catch (Exception e) {
			handleErrorAndFail(e, "Got exception checking declared classes");
		}
		try {
			if (!TestUtils.checkExtension(Class.forName("jtm.activity05.WaterRoad"),
					Class.forName("jtm.activity04.Road")))
				fail("WaterRoad does not extend Road");
			if (!TestUtils.checkExtension(Class.forName("jtm.activity05.Vehicle"),
					Class.forName("jtm.activity04.Transport")))
				fail("Vehicle does not extend Transport");
			if (!TestUtils.checkExtension(Class.forName("jtm.activity05.Ship"),
					Class.forName("jtm.activity04.Transport")))
				fail("Ship does not extend Transport");
			if (!(TestUtils.checkExtension(Class.forName("jtm.activity05.Amphibia"),
					Class.forName("jtm.activity04.Transport"))
					|| TestUtils.checkExtension(Class.forName("jtm.activity05.Amphibia"),
							Class.forName("jtm.activity05.Vehicle"))
					|| TestUtils.checkExtension(Class.forName("jtm.activity05.Amphibia"),
							Class.forName("jtm.activity05.Ship"))))
				fail("Amphibia does not extend neither Transport nor Vehicle nor Ship");

		} catch (Exception e) {
			handleErrorAndFail(e, "Got exception checking inheritance of classes");
		}
	}

	@Test
	public void test02CheckClasses() {
		try {
			Class.forName("jtm.activity05.Ship");
		} catch (Exception e) {
			handleErrorAndFail(e, "Ship class is not declared.");
		}
		try {
			Class.forName("jtm.activity05.Vehicle");
		} catch (Exception e) {
			handleErrorAndFail(e, "Vehicle class is not declared.");
		}
		try {
			Class.forName("jtm.activity05.WaterRoad");
		} catch (Exception e) {
			handleErrorAndFail(e, "WaterRoad class is not declared.");
		}
		try {
			Class.forName("jtm.activity05.Amphibia");
		} catch (Exception e) {
			handleErrorAndFail(e, "Amphibia class is not declared.");
		}
		logger.debug("OK");
	}
}