package jtm.activity05;

import static jtm.TestUtils.checkMethods;
import static jtm.TestUtils.handleErrorAndFail;
import static jtm.TestUtils.toParagraph;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Random;
import java.util.UUID;

import jtm.TestUtils;
import jtm.activity04.Road;
import jtm.activity04.Transport;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EncapsulationTest {
    private static Logger logger = Logger.getLogger(EncapsulationTest.class);

    static private byte sails = 4;
    static private float consumption = 10f;
    static private int tankSize = 55;
    static private int wheels = 6;
    static private Random randomGenerator;
    static private String id;

    @Test
    public void test00CheckDesign() {

        try {

            // Check, that all methods are implemented for tested classes
            String[] methods = {

                    "public jtm.activity05.Ship(java.lang.String,byte)",
                    "public float jtm.activity04.Transport.getConsumption()",
                    "public float jtm.activity04.Transport.getFuelInTank()",
                    "public int jtm.activity04.Transport.getTankSize()",
                    "public java.lang.String jtm.activity04.Transport.getId()",
                    "public java.lang.String jtm.activity04.Transport.toString()",
                    "public java.lang.String jtm.activity05.Ship.move(jtm.activity04.Road)"

            };
            assertEquals("Not all necessary methods are implemented for Ship. ", toParagraph(methods),
                    checkMethods(methods, "jtm.activity05.Ship"));
        } catch (Exception t) {
            handleErrorAndFail(t, "Could not get list of methods for Ship");
        }
        String[] methods = {"public jtm.activity05.Vehicle(java.lang.String,float,int,int)",
                "public float jtm.activity04.Transport.getConsumption()",
                "public float jtm.activity04.Transport.getFuelInTank()",
                "public int jtm.activity04.Transport.getTankSize()",
                "public java.lang.String jtm.activity04.Transport.getId()",
                "public java.lang.String jtm.activity04.Transport.toString()",
                "public java.lang.String jtm.activity05.Vehicle.move(jtm.activity04.Road)"

        };
        try {
            assertEquals("Not all necessary methods are implemented for Vehicle. ", toParagraph(methods),
                    checkMethods(methods, "jtm.activity05.Vehicle"));
        } catch (Exception t) {
            handleErrorAndFail(t, "Could not get list of methods for Vehicle");
        }
        try {
            String[] list = {"public jtm.activity05.Amphibia(java.lang.String,float,int,byte,int)",
                    "public float jtm.activity04.Transport.getConsumption()",
                    "public float jtm.activity04.Transport.getFuelInTank()",
                    "public int jtm.activity04.Transport.getTankSize()",
                    "public java.lang.String jtm.activity04.Transport.getId()",
                    "public java.lang.String jtm.activity04.Transport.toString()",
                    "public java.lang.String jtm.activity05.Amphibia.move(jtm.activity04.Road)"};
            assertEquals("Not all necessary methods are implemented for Amphibia. ", toParagraph(list),
                    checkMethods(list, "jtm.activity05.Amphibia"));
        } catch (Exception t) {
            handleErrorAndFail(t, "Could not get list of methods for Amphibia");
        }
        try {
            String[] list = {"public jtm.activity05.WaterRoad()",
                    "public jtm.activity05.WaterRoad(java.lang.String,java.lang.String,int)",
                    "public int jtm.activity04.Road.getDistance()",
                    "public java.lang.String jtm.activity04.Road.getFrom()",
                    "public java.lang.String jtm.activity04.Road.getTo()",
                    "public java.lang.String jtm.activity05.WaterRoad.toString()",
                    "public void jtm.activity04.Road.setDistance(int)",
                    "public void jtm.activity04.Road.setFrom(java.lang.String)",
                    "public void jtm.activity04.Road.setTo(java.lang.String)"};
            assertEquals("Not all necessary methods are implemented for WaterRoad. ", toParagraph(list),
                    checkMethods(list, "jtm.activity05.WaterRoad"));

        } catch (Exception t) {
            handleErrorAndFail(t, "Could not get list of methods for WaterRoad");
        }

    }

    @Test
    public void test01WaterRoad() {
        Road water = null;
        try {
            water = new WaterRoad("Rīga", "Ventspils", 240);
        } catch (Exception t) {
            handleErrorAndFail(t,
                    "Could not construct WaterRoad, check if it has constructor: WaterRoad(String, String, int).");
        }
        try {
            assertEquals("Road.toString() error", "WaterRoad Rīga — Ventspils, 240km", water.toString());
            logger.info("OK");
        } catch (Exception t) {
            handleErrorAndFail(t);
        }

    }

    @Test
    public void test02Transport() {

        Road earth = null, water = null;
        Transport ta = null;
        Vehicle tb = null;
        try {
            // Roads
            earth = new Road("Rīga", "Ogre", 37);
            water = new WaterRoad("Rīga", "Ventspils", 240);
            tb = new Vehicle(id + "2", 3f, 20, wheels);
            // Test casting
            ta = tb;
            // check that super() is used
            assertNotNull(ta.getId());
            // Test behavior
            logger.debug("ta:" + ta);
            // Check relation to fuel usage
            assertEquals("Transport test error.",
                    id + "2 Vehicle is driving on " + earth + " with " + wheels + " wheels",
                    ((Vehicle) ta).move(earth));
            assertEquals("Transport test error.", 18.89f, ta.getFuelInTank(), 0.1f);
            // Check, that cannot move on water as a vehicle
            assertEquals("Transport test error.", "Cannot drive on WaterRoad Rīga — Ventspils, 240km",
                    ((Vehicle) ta).move(water));
            logger.info("OK");
        } catch (Exception e) {
            handleErrorAndFail(e);
        }
    }

    @Test
    public void test03Ship() {
        Road earth = null, water = null;
        Transport ta = null, tb = null;
        try {
            // Roads
            earth = new Road("Rīga", "Ogre", 37);
            water = new WaterRoad("Rīga", "Ventspils", 240);
            // Ship
            ta = new Ship("Ship", (byte) 1);
            // check that super() is used
            assertNotNull(ta.getId());
            tb = new Ship(id + "3", sails);
            logger.debug("tb:" + tb);
            // Test casting
            ta = tb;
            // Test behavior
            assertEquals("Ship test error.", id + "3 Ship is sailing on " + water + " with " + sails + " sails",
                    ((Ship) tb).move(water));
            assertEquals("Ship test error.", "Cannot sail on Rīga — Ogre, 37km", ((Ship) tb).move(earth));
            // Test, that as a transport still sails on water
            assertEquals("Ship test error.", id + "3 Ship is sailing on " + water + " with " + sails + " sails",
                    tb.move(water));
            // Test, that as a transport still cannot move on earth
            assertEquals("Ship test error.", "Cannot sail on Rīga — Ogre, 37km", tb.move(earth));
            logger.info("OK");
        } catch (Exception e) {
            handleErrorAndFail(e);
        }
    }

    @Test
    public void test04Vehicle() {
        Road earth = null, water = null;
        Transport ta = null, tb = null;
        try {
            // Roads
            earth = new Road("Rīga", "Ogre", 37);
            water = new WaterRoad("Rīga", "Ventspils", 240);
            // Vehicle
            ta = new Vehicle("Vehicle", consumption, tankSize, wheels);
            // check that super() is used
            assertNotNull(ta.getId());
            tb = new Vehicle(id + "1", consumption, tankSize, wheels);
            // Test casting
            ta = tb;
            // Test behavior
            assertEquals("Vehicle fuel error.", 55, tb.getFuelInTank(), 0.1f);
            assertEquals("Vehicle fuel error.", id + "1 Vehicle is driving on " + earth + " with " + wheels + " wheels",
                    ((Vehicle) tb).move(earth));
            assertEquals("Vehicle fuel consumption error.", 51.3f, ta.getFuelInTank(), 0.1f);
            // Test, that as a transport still drives on earth
            assertEquals("Vehicle movement error.",
                    id + "1 Vehicle is driving on " + earth + " with " + wheels + " wheels", tb.move(earth));
            assertEquals("Vehicle fuel consumption error.", 47.6f, ta.getFuelInTank(), 0.1f);
            // Test, that as a Vehicle still cannot move on water
            assertEquals("Vehicle movement error.", "Cannot drive on WaterRoad Rīga — Ventspils, 240km",
                    tb.move(water));
            assertEquals("Vehicle fuel consumption error.", 47.6f, ta.getFuelInTank(), 0.1f);
            logger.info("OK");
        } catch (Exception e) {
            handleErrorAndFail(e);
        }
    }

    @Test
    public void test05Amphibia() {
        Road earth = null, water = null;
        Transport ta = null, tb = null;
        Amphibia a = null;
        try {
            // Roads
            earth = new Road("Rīga", "Ogre", 37);
            water = new WaterRoad("Rīga", "Ventspils", 240);
            // Amphibia
            // Constructor with parameters
            tb = new Amphibia(id, 4f, 40, sails, wheels);
            logger.debug("tb:" + tb);

            // Check all couplings between Transport and Ship/Vehicle
            assertEquals("Amphibia construction error.", id, tb.getId());
            assertEquals("Amphibia fuel error.", 40, tb.getFuelInTank(), 0.1f);

            // Asserts
            assertEquals("Amphibia movement error.",
                    id + " Amphibia is driving on " + earth + " with " + wheels + " wheels", tb.move(earth));
            assertEquals("Amphibia fuel error.", 38.5, tb.getFuelInTank(), 0.1f);
            assertEquals("Amphibia movement error.",
                    id + " Amphibia is sailing on " + water + " with " + sails + " sails", tb.move(water));
            // Check that fuel is not used when sailing
            assertEquals("Amphibia fuel error.", 38.5, tb.getFuelInTank(), 0.1f);

        } catch (Exception e) {
            handleErrorAndFail(e);
        }
        try {
            // Check that casting to Amphibia works
            a = (Amphibia) tb;
            logger.debug(a);
        } catch (Exception e) {
            fail("Could not cast Amphibia to Amphibia");
        }
        // Check that Amphibia is Transport
        try {
            ta = tb;
        } catch (Exception e) {
            fail("Amphibia is not implemented as a transport");
        }
        logger.info("OK");

    }

    @Test
    public void test06Encapsulation() {
        TestUtils tu = new TestUtils();
        Transport ta = null;
        // Vehicle
        ta = new Vehicle("Vehicle", consumption, tankSize, wheels);
        try {
            logger.info(tu.listFields(ta));
            fail("Vehicle properties are not protected.");
        } catch (Exception e) {
            assertEquals("Vehicle properties are not protected.", e.getClass(), IllegalAccessException.class);
        }
        // Ship
        ta = new Ship("Ship", sails);
        try {
            logger.info(tu.listFields(ta));
            fail("Ship properties are not protected.");
        } catch (Exception e) {
			assertEquals("Ship properties are not protected.", e.getClass(), IllegalAccessException.class);
        }
        // Amphibia
        ta = new Amphibia("Amphibia", consumption, tankSize, sails, wheels);
        try {
            logger.info(tu.listFields(ta));
            fail("Amphibia properties are not private.");
        } catch (Exception e) {
			assertEquals("Amphibia properties are not private.", e.getClass(), IllegalAccessException.class);
        }
    }

    @Before
    public void setUp() {
        randomGenerator = new Random();
        // Transport properties
        wheels = randomGenerator.nextInt(10) + 1;
        sails = (byte) (randomGenerator.nextInt(10) + 1);
        id = UUID.randomUUID().toString().substring(0, 4);
    }
}
