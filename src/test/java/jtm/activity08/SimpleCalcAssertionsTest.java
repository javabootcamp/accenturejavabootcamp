package jtm.activity08;

import org.apache.log4j.Logger;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class SimpleCalcAssertionsTest {
    private static Logger logger = Logger.getLogger(SimpleCalcAssertionsTest.class);


    @Test
    public void testAddInRange() {
        assertEquals(Integer.valueOf(3), SimpleCalc.add(1, 2));
        assertEquals(Integer.valueOf(-3), SimpleCalc.add(-1, -2));
        assertEquals(Integer.valueOf(10), SimpleCalc.add(5, 5));
        assertEquals(Integer.valueOf(-10), SimpleCalc.add(-5, -5));
    }

    @Test
    public void testAddOutOfRange() {
        assertNull(SimpleCalc.add(7, 4));
        assertNull(SimpleCalc.add(4, 8));
        assertNull(SimpleCalc.add(-7, -4));
        assertNull(SimpleCalc.add(-7, -8));
    }

    @Test
    public void testSubtractInRange() {
        assertEquals(Integer.valueOf(1), SimpleCalc.subtract(-1, -2));
        assertEquals(Integer.valueOf(8), SimpleCalc.subtract(10, 2));
        assertEquals(Integer.valueOf(-8), SimpleCalc.subtract(2, 10));
        assertEquals(Integer.valueOf(0), SimpleCalc.subtract(0, 0));
    }

    @Test
    public void testSubtractOutOfRange() {
        assertNull(SimpleCalc.subtract(10, 11));
        assertNull(SimpleCalc.subtract(11, 8));
    }


    @Test
    public void testMultiplyInRange() {
        assertEquals(Integer.valueOf(9), SimpleCalc.multiply(3, 3));
        assertEquals(Integer.valueOf(4), SimpleCalc.multiply(2, 2));
        assertEquals(Integer.valueOf(-9), SimpleCalc.multiply(-3, 3));
        assertEquals(Integer.valueOf(0), SimpleCalc.multiply(0, 0));
    }

    @Test
    public void testMultiplyOutOfRange() {
        assertNull(SimpleCalc.multiply(5, 3));
        assertNull(SimpleCalc.multiply(-4, 3));
    }

    @Test
    public void testDivideInRange() {
        assertEquals(Integer.valueOf(3), SimpleCalc.divide(9, 3));
        assertEquals(Integer.valueOf(1), SimpleCalc.divide(2, 2));
        assertEquals(Integer.valueOf(-1), SimpleCalc.divide(-3, 3));
    }

    @Test
    public void testDivideOutOfRange() {
        assertNull(SimpleCalc.divide(11, 3));
        assertNull(SimpleCalc.divide(-4, 12));
    }

}
