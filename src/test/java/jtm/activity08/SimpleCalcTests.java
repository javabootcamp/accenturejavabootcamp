package jtm.activity08;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(value = { ClassesTest.class, SimpleCalcAssertionsTest.class})
public class SimpleCalcTests {

}
