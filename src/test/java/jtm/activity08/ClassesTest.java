package jtm.activity08;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.assertEquals;

import jtm.TestUtils;
import org.junit.Test;

public class ClassesTest {

	@Test
	public void test01Class() {
		try {
			Class.forName("jtm.activity08.SimpleCalcException");
		} catch (Exception e) {
			handleErrorAndFail(e, "SimpleCalcException class is not declared.");
		}
		try {
			TestUtils.checkExtension(Class.forName("jtm.activity08.SimpleCalcException"), Exception.class);
		} catch (Exception e) {
			handleErrorAndFail(e, "SimpleCalcException class doesn't extend Exception.");
		}
	}

	@Test
	public void test02Constructors() {
		try {
			assertEquals("Not all necessary constructors are implemented for SimpleCalcException. ",
					"public jtm.activity08.SimpleCalcException(java.lang.String)\n"
							+ "public jtm.activity08.SimpleCalcException(java.lang.String,java.lang.Throwable)",
					TestUtils.getConstructors("jtm.activity08.SimpleCalcException"));
		} catch (Exception e) {
			handleErrorAndFail(e, "Could not get list of constructors for SimpleCalcException");
		}

	}

}
