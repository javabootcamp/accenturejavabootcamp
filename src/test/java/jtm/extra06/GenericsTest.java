
package jtm.extra06;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import jtm.extra06.Generics;
import org.junit.Before;
import org.junit.Test;

public class GenericsTest {

	private static Logger logger = Logger.getLogger(GenericsTest.class);
	public static StringBuilder log;
	static Generics<Number> testCase;
	private static final double DELTA = 1e-15;

	@Before
	public void resetLog() {
		log = new StringBuilder("");
		log.setLength(0);
		logger.debug("resetLog");
	}

	@Test
	public void testPush() {
		try {
			testCase = new Generics<Number>();
			double dbl = TestUtils.randomDbl(10);
			testCase.push(dbl);
			assertEquals("Push test error.", "Generics instance created\njava.lang.Double: " + dbl + " pushed\n",
					log.toString());
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

	@Test
	public void testPop() {
		try {
			testCase = new Generics<Number>();
			double dbl = TestUtils.randomDbl(10);
			testCase.push(dbl);
			double test = (Double) testCase.pop();
			assertEquals(dbl, GenericsTest.DELTA, test);
			assertTrue(testCase.isEmpty());
			assertEquals("Pop test error.", "Generics instance created\njava.lang.Double: " + dbl
					+ " pushed\njava.lang.Double: " + dbl + " popped\n", log.toString());
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

	@Test
	public void testTypes() {
		testCase = new Generics<Number>();
		Random rnd = new Random();
		Double d = rnd.nextDouble() * 100;
		Integer i = rnd.nextInt() * 100;
		Long l = rnd.nextLong() * 100;
		Float f = rnd.nextFloat() * 100;
		try {
			testCase.push(d);
			testCase.push(i);
			testCase.push(l);
			testCase.push(f);
			while (!testCase.isEmpty()) {
				testCase.pop();
			}
			assertTrue(testCase.isEmpty());
			assertEquals("Inserted data type error.",
					("Generics instance created\njava.lang.Double: " + d + " pushed\njava.lang.Integer: " + i
							+ " pushed\njava.lang.Long: " + l + " pushed\njava.lang.Float: " + f
							+ " pushed\njava.lang.Float: " + f + " popped\njava.lang.Long: " + l
							+ " popped\njava.lang.Integer: " + i + " popped\njava.lang.Double: " + d + " popped\n"),
					log.toString());
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}
}
