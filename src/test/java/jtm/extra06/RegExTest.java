
package jtm.extra06;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.apache.maven.shared.utils.StringUtils;
import jtm.extra06.RegEx;
import org.junit.Before;
import org.junit.Test;

public class RegExTest {

	private static Logger logger = Logger.getLogger(RegExTest.class);
	RegEx testCase;

	@Before
	public void setUp() {
		this.testCase = new RegEx();
	}

	/**
	 * Test method for
	 * {@link jtm.extra06.RegEx#isLuckyNumber(java.lang.String)}.
	 */
	@Test
	public void testIsLuckyNumber() {
		try {
			assertTrue("Regex check error.", !testCase.isLuckyNumber("sd9d8a7c56"));
			assertTrue("Regex check error.", testCase.isLuckyNumber("9D DS DS DSDS  9DSdf7"));
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

	/**
	 * Test method for {@link jtm.extra06.RegEx#findKenny(java.lang.String)}.
	 */
	@Test
	public void testFindKenny() {
		String n = StringUtils.repeat(TestUtils.randomWord("n", "l"), TestUtils.randomInt(10, 2));
		String pad = StringUtils.repeat(
				TestUtils.randomWord("", " ", "\t", "\\", "'", "/", "«", "”", "-", "0", "1", "9", "a", "Ā", "š", "Š"),
				TestUtils.randomInt(10, 0));
		int padLength = pad.length();
		try {
			assertEquals("Regex check error.", -1,
					testCase.findKenny(pad + "Mary-Janny-{____}Bob, Scally +-=Scott" + pad));
			assertEquals("Regex check error.", -1, testCase.findKenny(pad));
			assertEquals("Regex check error.", -1, testCase.findKenny(pad + "Key" + pad));
			assertEquals("Regex check error.", -1, testCase.findKenny(pad + "Keny" + pad));
			assertEquals("Regex check error.", 0, testCase.findKenny("Ke" + n + "y" + pad));
			assertEquals("Regex check error.", padLength, testCase.findKenny(pad + "Ke" + n + "y" + pad));
			assertEquals("Regex check error.", 3 * padLength + 6,
					testCase.findKenny(pad + "Ke" + pad + "anny" + pad + "Ke" + n + "y"));
			assertEquals("Regex check error.", 3 * padLength + 6,
					testCase.findKenny(pad + "Ke" + pad + "anny" + pad + "Ke" + n + "y" + pad + "Ke" + n + "y" + pad));
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

	/**
	 * Test method for {@link jtm.extra06.RegEx#isGood(java.lang.String)}.
	 */
	@Test
	public void testIsGood() {
		try {
			assertTrue("Regex check error.", !testCase.isGood("DSDSDcxzczx"));
			assertTrue("Regex check error.", testCase.isGood("67425482"));
			assertTrue("Regex check error.", !testCase.isGood("674254821"));
			assertTrue("Regex check error.", testCase.isGood("66425482"));
			assertTrue("Regex check error.", !testCase.isGood("664254821"));
			assertTrue("Regex check error.", !testCase.isGood("864254821"));
			assertTrue("Regex check error.", testCase.isGood("+37167425482"));
			assertTrue("Regex check error.", !testCase.isGood("+32167425482"));
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}
}
