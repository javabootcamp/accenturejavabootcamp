
package jtm.extra06;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import jtm.extra06.Holiday;
import org.junit.Test;

public class HolidayTest {

	private static Logger logger = Logger.getLogger(HolidayTest.class);

	/**
	 * Test method for {@link jtm.extra06.Holiday#getNearest(int, int)}.
	 */
	@Test
	public void testGetNearest() {
		try {
			assertEquals(Holiday.NEW_YEAR, Holiday.getNearest(12, 31));
			assertEquals(Holiday.CHUCK_NORRIS_BIRTHSDAY, Holiday.getNearest(3, 9));
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}
}
