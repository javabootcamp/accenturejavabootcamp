package jtm.extra17;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/*

Your task is to determine if the cards in the list makes up a straight
(five cards of sequential rank) or not.
The cards will always have values ranging from 2-14, where 14 is the ace.
Be aware that the ace (14) also should count as value 1!
The number of cards will vary, but will never be more than 7 (the board (5) + player hand (2))

 Examples:
    straight: 9-10-11-12-13
    straight: 14-2-3-4-5
    straight: 2-7-8-5-10-9-11
    not straight: 7-8-12-13-14
 */
public class PokerStraightTest {

    @Test
    public void test1IsStraight() {
        ArrayList<Integer> hand = new ArrayList<Integer>();
        hand.add(2);
        hand.add(3);
        hand.add(4);
        hand.add(5);
        hand.add(6);
        assertTrue("2, 3, 4, 5, 6", PokerStraight.isStraight(hand));
    }

    @Test
    public void test2IsStraight() {
        ArrayList<Integer> hand = new ArrayList<Integer>();
        hand.add(14);
        hand.add(5);
        hand.add(4);
        hand.add(2);
        hand.add(3);
        assertTrue("14, 5, 4 ,2, 3", PokerStraight.isStraight(hand));
    }

    @Test
    public void test3IsStraight() {
        ArrayList<Integer> hand = new ArrayList<Integer>();
        hand.add(2);
        hand.add(3);
        assertFalse("2, 3", PokerStraight.isStraight(hand));
    }

    @Test
    public void test4IsStraight() {
        ArrayList<Integer> hand = new ArrayList<Integer>();
        hand.add(7);
        hand.add(7);
        hand.add(12);
        hand.add(11);
        hand.add(3);
        hand.add(4);
        hand.add(14);
        assertFalse("7, 7, 12 ,11, 3, 4, 14", PokerStraight.isStraight(hand));
    }
}
