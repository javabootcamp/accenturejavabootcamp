package jtm.extra08;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import jtm.extra08.Invoice;
import jtm.extra08.InvoiceManager;
import jtm.extra08.Item;
import org.junit.Test;

public class InvoiceManagerTest {
	static InvoiceManager im;
	private static Logger logger = Logger.getLogger(InvoiceManagerTest.class);

	@Test
	public void testInvoiceToItem() {
		im = new InvoiceManager();
		im.clearData();
		// Test CRUD operations starting from invoice, then adding items to it
		Invoice invoice1 = im.createInvoice(1);
		Item item1 = im.createInvoiceItem(invoice1, 1, "Name1", 1.1f, 1);
		assertEquals("Item value comparison error", (Integer) 1, item1.getId());
		assertEquals("Item value comparison error", "Name1", item1.getName());
		assertEquals("Item value comparison error", 1.1f, item1.getPrice(), 0.0001);
		assertEquals("Item value comparison error", (Integer) 1, item1.getCount());
		Item item2 = im.createInvoiceItem(invoice1, 2, "Name2", 2.2f, 2);
		assertEquals("Item value comparison error", (Integer) 2, item2.getId());
		assertEquals("Item value comparison error", "Name2", item2.getName());
		assertEquals("Item value comparison error", 2.2f, item2.getPrice(), 0.0001);
		assertEquals("Item value comparison error", (Integer) 2, item2.getCount());

		// Check invoice values
		im = new InvoiceManager();
		assertEquals("Invoice value error.", (Integer) 1, getInvoiceId(invoice1));
		assertEquals("Invoice value error.", (Integer) 2, getInvoiceItemNo(invoice1));
		assertEquals("Invoice value error.", 5.5f, getInvoiceTotalAmount(invoice1), 0.1f);

		Invoice invoice11 = im.searchInvoice(1);
		assertEquals("Invoice value error.", (Integer) 1, getInvoiceId(invoice11));
		assertEquals("Invoice value error.", (Integer) 2, getInvoiceItemNo(invoice11));
		assertEquals("Invoice value error.", (Float) 5.5f, getInvoiceTotalAmount(invoice11));
		assertEquals("Invoice value error.", (Integer) 2, getInvoiceItemNo(invoice11));
		assertEquals("Invoice value error.", invoice1.toString(), invoice11.toString());

		// Check values of items

		Item item11 = im.searchItem(1);
		assertEquals("Item value error.", (Integer) 1, getItemId(item11));
		assertEquals("Item value error.", "Name1", getItemName(item11));
		assertEquals("Item value error.", (Float) 1.1f, getItemPrice(item11));
		assertEquals("Item value error.", (Integer) 1, getItemCount(item11));

		Item item21 = im.searchItem(2);
		assertEquals("Item value error.", (Integer) 2, getItemId(item21));
		assertEquals("Item value error.", "Name2", getItemName(item21));
		assertEquals("Item value error.", 2.2f, getItemPrice(item21), 0.1);
		assertEquals("Item value error.", (Integer) 2, getItemCount(item21));

		logger.debug(invoice11);
		logger.info("OK");

	}

	@Test
	public void testItemToInvoice() {
		// Check creation of items before and then adding them to invoice after
		Invoice invoice2 = new Invoice();
		invoice2.setId(2);
		im.persist(invoice2);

		Item item3 = new Item();
		item3.setId(3);
		item3.setName("Name3");
		item3.setPrice(3.3f);
		item3.setCount(3);
		item3.setInvoice(invoice2);
		im.persist(item3);

		Item item4 = new Item();
		item4.setId(4);
		item4.setName("Name4");
		item4.setPrice(4.4f);
		item4.setCount(4);
		item4.setInvoice(invoice2);
		im.persist(item4);

		// Check invoice
		Invoice invoice21 = im.searchInvoice(2);
		assertEquals("Invoice value error.", getInvoiceId(invoice2), getInvoiceId(invoice21));
		assertEquals("Invoice value error.", (Integer) 2, getInvoiceItemNo(invoice21));
		assertEquals("Invoice value error.", getInvoiceTotalAmount(invoice2), getInvoiceTotalAmount(invoice21));
		assertEquals("Invoice value error.", getInvoiceItems(invoice2), getInvoiceItems(invoice21));
		assertEquals("Invoice value error.", invoice2.toString(), invoice21.toString());
		assertEquals("Invoice value error.", invoice2, invoice21);

		logger.debug(invoice21);
		logger.info("OK");
	}

	// Invoice accessors

	Integer getInvoiceId(Invoice invoice) {
		return (Integer) TestUtils.invokeGetMethod(invoice, "jtm.extra08.Invoice", "getId");
	}

	Integer getInvoiceItemNo(Invoice invoice) {
		return (Integer) TestUtils.invokeGetMethod(invoice, "jtm.extra08.Invoice", "getItemNo");
	}

	Float getInvoiceTotalAmount(Invoice invoice) {
		return (Float) TestUtils.invokeGetMethod(invoice, "jtm.extra08.Invoice", "getTotalAmount");
	}

	@SuppressWarnings("unchecked")
	Set<Item> getInvoiceItems(Invoice invoice) {
		return (Set<Item>) TestUtils.invokeGetMethod(invoice, "jtm.extra08.Invoice", "getItems");
	}

	// Item accessors

	Integer getItemId(Item item) {
		return (Integer) TestUtils.invokeGetMethod(item, "jtm.extra08.Item", "getId");
	}

	String getItemName(Item item) {
		return (String) TestUtils.invokeGetMethod(item, "jtm.extra08.Item", "getName");
	}

	Float getItemPrice(Item item) {
		return (Float) TestUtils.invokeGetMethod(item, "jtm.extra08.Item", "getPrice");
	}

	Integer getItemCount(Item item) {
		return (Integer) TestUtils.invokeGetMethod(item, "jtm.extra08.Item", "getCount");
	}

	Invoice getItemInvoice(Item item) {
		return (Invoice) TestUtils.invokeGetMethod(item, "jtm.extra08.Item", "getInvoice");
	}
}
