package jtm.extra03;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runners.MethodSorters;

import jtm.TestUtils;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PracticalNumbersTest {
	private static Logger logger = Logger.getLogger(PracticalNumbersTest.class);
	private static PracticalNumbers p;
	private static int from, to;

	@Rule
	public Timeout globalTimeout = Timeout.seconds(1);

	@BeforeClass
	public static void beforeClass() {
		p = new PracticalNumbers();

	}

	@Test()
	public void test1getPracticalNumbers() {
		from = TestUtils.randomInt(50, 500);
		to = TestUtils.randomInt(50, 550);
		logger.debug("Range: " + from + ".." + to);
		String result;
		String expected;
		try {
			expected = getPracticalNumbers(from, to);
			logger.debug("Expected: " + expected);
			result = p.getPracticalNumbers(from, to);
			logger.debug("Result: " + result);
			assertEquals(expected, result);
		} catch (Exception e) {
			TestUtils.handleErrorAndFail(e);
		}
	}

	@Test()
	public void test2ImplementationSpeed() {
		from = TestUtils.randomInt(50, 20000);
		to = TestUtils.randomInt(50, 20050);
		logger.debug("Range: " + from + ".." + to);
		String result;
		String expected;
		try {
			expected = getPracticalNumbers(from, to);
			logger.debug("Expected: " + expected);
			result = p.getPracticalNumbers(from, to);
			logger.debug("Result: " + result);
			assertEquals(expected, result);
		} catch (Exception e) {
			TestUtils.handleErrorAndFail(e);
		}
	}

	private String getPracticalNumbers(int from, int to) {
		int a[] = new int[to - from];
		int indx = 0;
		for (int i = from; i <= to; i++) {
			int sum = 0;
			for (int j = 1; j <= i / 2; j++) {
				if (i % j == 0) {
					if (sum < j - 1)
						continue;
					sum += j;
				}
			}
			if (sum >= i - 1) {
				a[indx++] = i;
			}
		}
		int count = 0;
		for (int i = 0; i < a.length; i++) {
			if (a[i] != 0)
				count++;
		}
		int ans[] = new int[count];
		for (int i = 0; i < count; i++) {
			ans[i] = a[i];
		}
		return Arrays.toString(ans);
	}

}
