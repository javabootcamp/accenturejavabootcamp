
package jtm.extra05;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.google.common.base.Strings;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class XMLCarsTest {
	public static final String RELATIVE_PATH_TO_GENERATED_XML = "./src/main/java/jtm/extra05/cars.xml";
	public static final String RELATIVE_PATH_TO_XSD = "./src/main/java/jtm/extra05/cars.xsd";

	private static Logger logger = Logger.getLogger(XMLCarsTest.class);
	private static XMLCars xmlCars;
	private static Document doc;

	private static String model;
	private static String color;
	private static String notes;
	private static int year;
	private static float price;
	private static int id;
	private static String sid;
	private static String[] escStart = { "<", "'", "\"", "'", "\"", "&", "\\", "/", "«", "“", "->", "<!--" };
	private static String[] escEnd = { ">", "'", "\"", "\"", "'", "&", "/", "\\", "»", "”", "<-", "-->" };
	private static int escNo;

	private static Element cars;

	@BeforeClass
	public static void BeforeClass() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
		String cdate = df.format(new Date());
		escNo = TestUtils.randomInt(escStart.length - 1, 0);
		id = TestUtils.randomInt(1500, 0);
		model = escStart[escNo]
				+ TestUtils.randomWord("Audi", "BMW", "GM", "Ford", "Toyota", "Miver&ga", "Ла́да", "Özaltin")
				+ escEnd[escNo];
		color = TestUtils.randomWord("White", "Black", "Red", "Green", "Blue", "");
		notes = escStart[escNo] + cdate + escEnd[escNo];
		year = TestUtils.randomInt(25, 1990);
		price = TestUtils.randomInt(500000, 500000) / 100f;
	}

	@Test
	public void test2validateXMLPositive() {
		// Positive test
		try {
			xmlCars = new XMLCars();
			for (int i = 0; i < TestUtils.randomInt(10, 1); i++) {
				id++;
				xmlCars.addCar(id, model, color, year, price, notes);
				addCar(id, model, color, year, price, notes);

			}
			assertEquals("Produced XML is wrong.", getXML(), xmlCars.getXML());
			assertTrue("Validation of produced XML failed.", XMLCars.validateXMLSchema(readXSD(), xmlCars.getXML()));
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
		logger.info("OK");
	}

	@Test
	public void test3validateXMLNegative() {
		// Positive test
		try {
			XMLCars.validateXMLSchema(readXSD(), xmlCars.getXML() + "A");
			fail("Failed to catch Exception when testing broken XML");
		} catch (Exception e) {
			assertEquals("Wrong message for validation exeption.", "Content is not allowed in trailing section.",
					e.getMessage());
		}
		try {
			XMLCars.validateXMLSchema(readXSD(), xmlCars.getXML().replaceAll("id=\"", "number=\""));
			fail("Failed to catch Exception when testing broken XML");
		} catch (Exception e) {
			assertEquals("Wrong message for validation exeption.",
					"cvc-complex-type.3.2.2: Attribute 'number' is not allowed to appear in element 'car'.",
					e.getMessage());
		}
		try {
			sid = Strings.padStart(Integer.toString(id), 4, '0');
			XMLCars.validateXMLSchema(readXSD(), xmlCars.getXML().replaceAll("id=\"" + sid + "\"", "id=\"WrongID\""));
			fail("Failed to catch Exception when testing broken XML");
		} catch (Exception e) {
			assertEquals("Wrong message for validation exeption.",
					"cvc-pattern-valid: Value 'WrongID' is not facet-valid with respect to pattern '[0-9]{4}' for type '#AnonType_idcarcars'.",
					e.getMessage());
		}
		logger.info("OK");
	}

	private String readXSD() throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(RELATIVE_PATH_TO_XSD));
		return new String(encoded, "UTF-8");
	}

	private void addCar(int id, String model, String color, int year, float price, String notes) throws Exception {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		if (doc == null) {
			doc = docBuilder.newDocument();
			cars = doc.createElement("cars");
			doc.appendChild(cars);
		}

		// car element
		Element car = doc.createElement("car");
		cars.appendChild(car);

		// car attributes
		String sid = Strings.padStart(Integer.toString(id), 4, '0');
		car.setAttribute("id", sid);
		car.setAttribute("notes", notes);

		// model element
		Element model1 = doc.createElement("model");
		model1.appendChild(doc.createTextNode(model));
		car.appendChild(model1);

		// color element
		Element color1 = doc.createElement("color");
		color1.appendChild(doc.createTextNode(color));
		car.appendChild(color1);

		// year element
		Element year1 = doc.createElement("year");
		year1.appendChild(doc.createTextNode(Integer.toString(year)));
		car.appendChild(year1);

		// Price element
		Element price1 = doc.createElement("price");
		price1.appendChild(doc.createTextNode(Float.toString(price)));
		car.appendChild(price1);

		// Comments
		Comment comment = doc.createComment(notes);
		car.appendChild(comment);

	}

	private String getXML() throws Exception {

		// change doc source to String
		DOMSource domSource = new DOMSource(doc);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();

		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

		transformer.transform(domSource, result);
		logger.debug("XML\n" + writer.toString());
		return writer.toString();
	}

}
