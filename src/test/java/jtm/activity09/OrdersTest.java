package jtm.activity09;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.NoSuchElementException;
import java.util.Set;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OrdersTest {

	static Orders orders;
	private static Logger logger = Logger.getLogger(OrdersTest.class);
	static int customerCount;
	static int itemCount;
	static String customer;
	static String item;
	static String orderString;
	static String orderStringSorted;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TestUtils.checkOS();
		OrderTest.beforeClass();
		customer = OrderTest.customer1;
		item = OrderTest.item1;
		customerCount = TestUtils.randomInt(3, 3);
		itemCount = TestUtils.randomInt(4, 4);
	}

	@Test
	public final void test01OrderListInitialization() {
		orders = new Orders();
		assertNotNull("Order list is not initialized.", orders);
		assertEquals("hasNext() for empty list of orders should return false.", false, orders.hasNext());
		try {
			orders.next();
			fail("NoSuchElementException was suspected for next() on empty Orders.");
		} catch (NoSuchElementException e) {
			logger.debug("Got NoSuchElementException as suspected");
		}
		logger.info("OK");
	}

	@Test
	public final void test02GetItemsSetForEmptyList() {
		orders = new Orders();
		Set<Order> ordersSet = orders.getItemsSet();
		assertNotNull("If there are no orders, empty set should be returned instead of null", ordersSet);
		assertEquals("getItemsSet() should be 0, if OrdersList is empty", 0, ordersSet.size());
		logger.info("OK");
	}

	@Test
	public final void test03Add() {
		addItems(orders, customerCount, itemCount);
		orderString = formatList(orders.toString());
		logger.debug("Initial list of orders:\n" + orders);
		assertTrue("List content error. Orders.toString() should match '.*" + item + ".*" + customer + ".*'",
				orders.toString().matches(".*" + item + ".*" + customer + ".*"));
		assertEquals("Wrong content of the list, or wrongly implemented .getItemsList() and .toString() methods.",
				orders.getItemsList().toString(), orders.toString());
		assertEquals("Wrong size of the list.", orders.getItemsList().size(), (itemCount * customerCount));
		logger.info("OK");
	}

	@Test
	public final void test04Iterations() {
		String iterationString = "";
		for (int i = 0; i < itemCount * customerCount; i++) {
			assertTrue("hasNext() error. + i", orders.hasNext());
			iterationString = iterationString + "\n" + orders.next().toString();
			System.out.println(iterationString);
		}
		logger.debug("Values of elements from iterations:" + iterationString);
		assertEquals("List value comparison error.", orderString, "[" + iterationString + "\n]");
		logger.info("OK");
	}


	@Test
	public final void test06Remove() {
		Orders orders = new Orders();
		// Negative .remove() test on empty list
		try {
			orders.remove();
			fail("NoSuchElementException was suspected for remove() on empty Orders.");
		} catch (IllegalStateException e) {
			logger.debug("Got IllegalStateException as suspected");
		}
		int customerCount = 3;
		int itemCount = 2;

		// Add items to the list
		addItems(orders, customerCount, itemCount);
		for (int i = customerCount; i > 0; i--) {
			for (int j = itemCount; j > 0; j--) {
				assertTrue("hasNext() error.", orders.hasNext());
				// Check that each particular item can be removed
				System.out.println(item + j + ": " + customer + i + ": " + ((j * i) + i) + " order:"
						+ orders.next().toString());
				orders.remove();
			}
		}
		// Check that list is empty at the end
		assertEquals("List size error", 0, orders.getItemsList().size());
		assertEquals("List content error", "[]", orders.toString());

		// Negative tests on empty list after removal of all elements
		try {
			orders.next();
			fail("NoSuchElementException was suspected for next() on empty Orders.");
		} catch (NoSuchElementException e) {
			logger.debug("Got NoSuchElementException as suspected");
		}
		try {
			orders.remove();
			fail("NoSuchElementException was suspected for remove() on empty Orders.");
		} catch (IllegalStateException e) {
			logger.debug("Got IllegalStateException as suspected");
		}

		// Test that Orders can be added to list again
		try {
			orders.add(new Order(customer, item, 1));
			orders.next();
			orders.remove();
		} catch (Exception e) {
			fail("Error when adding/removing Order on emptied list.\n" + e);
		}

		logger.info("OK");
	}

	@Test
	public final void test07GetItemsSet() {
		orders = new Orders();
		addItems(orders, 3, 3, "TheCustomer", "TheItem");
		logger.debug("List of items for itmem set: \n" + (formatList(orders.toString())));
		String result = formatList(orders.getItemsSet().toString());
		logger.debug("Items set:\n" + result);
		logger.info("OK");
	}

	private void addItems(Orders orders, int customerCount, int itemCount) {
		addItems(orders, customerCount, itemCount, customer, item);
	}

	private void addItems(Orders orders, int customerCount, int itemCount, String customer, String item) {
		for (int i = customerCount; i >= 1; i--) {
			for (int j = itemCount; j >= 1; j--) {
				Order order = new Order(customer + j, item + i, j * i + i);
				orders.add(order);
			}
		}
	}

	private String formatList(String listString) {
		listString = listString.replace(", ", "\n");
		listString = listString.replace("[", "[\n");
		listString = listString.replace("]", "\n]");
		return listString;
	}
}
