package jtm.activity09;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

public class OrderTest {
	static String customer1;
	static String customer2;
	static String item1;
	static String item2;
	static Integer count1;
	static Integer count2;
	static Order order1, order2;

	private static Logger logger = Logger.getLogger(OrderTest.class);

	@BeforeClass
	public static void beforeClass() {
		customer1 = TestUtils.randomWord("Arvīds", "Andris", "Aigars", "Aldonis", "Ansis");
		customer2 = TestUtils.randomWord("Marija", "Made", "Maija", "Māra", "Monika");
		item1 = TestUtils.randomWord("Milk", "Water", "Vodka", "Beer", "Juice");
		item2 = TestUtils.randomWord("Cookies", "Coffee", "Tea", "Vobla", "Cake");
		count1 = TestUtils.randomInt(4, 1);
		count2 = TestUtils.randomInt(5, 6);

		order1 = new Order(customer1, item1, count1);
		order2 = new Order(customer2, item2, count2);
	}

	@Test
	public final void test02Equals() {
		try {

			assertNotEquals("equals() error. Comparison with any other object is unequal.", order1, new Object());
			assertEquals("equals() error. Orders with the same values should be considered equal.", order1,
					new Order(customer1, item1, count1));
			assertNotEquals("equals() error. Orders with the different values should be considered unequal.", order1,
					new Order(customer1, item1, count2));
			assertNotEquals("equals() error. Orders with the different values should be considered unequal.", order1,
					new Order(customer1, item2, count1));
			assertNotEquals("equals() error. Orders with the different values should be considered unequal.", order1,
					new Order(customer2, item1, count1));
			assertNotEquals("equals() error. Orders with the different values should be considered unequal.", order1,
					new Order(customer2, item2, count2));
			assertEquals("compareTo() error. Orders with the same values should be considered equal.", 0,
					order1.compareTo(new Order(customer1, item1, count1)));

			logger.info("OK");
		} catch (Exception e) {
			TestUtils.handleErrorAndFail(e);
		}
	}

	@Test
	public final void test03Hash() {
		Order order1 = new Order(customer1, item1, count1);
		Order order2 = new Order(customer1, item1, count1);
		Order order3 = new Order(customer2, item1, count1);
		int hash1 = order1.hashCode();
		int hash2 = order2.hashCode();
		logger.debug("Hash1: " + hash1 + " hash2:" + hash2);
		assertEquals("Orders with the same values should be considered equal.", order1, order2);
		assertEquals("Hashes of equal orders are not the same.", hash1, hash2);
		assertTrue("Hashes of different orders are not different.", hash1 != order3.hashCode());
		logger.info("OK");
	}

	@Test
	public final void test04CompareTo() {
		try {
			// The same
			Order order1 = new Order(customer1, item1, count1);
			Order order2 = new Order(customer1, item1, count1);
			assertEquals("compareTo() error. Orders " + order1 + " " + order2 + " should be considered equal.", 0,
					order1.compareTo(order2));
			assertEquals("compareTo() error. Orders " + order2 + " " + order1 + " should be considered equal.", 0,
					order2.compareTo(order1));

			// Different
			Order orderA = new Order("OrdererA", "ItemNameA", 10);

			Order orderB = new Order("OrdererA", "ItemNameA", 10);
			assertEquals("compareTo() error. Orders " + orderA + " " + orderB + " should be considered equal.", 0,
					orderA.compareTo(orderB));

			// Second order larger
			// Different customer
			orderB = new Order("OrdererB", "ItemNameA", 10);
			assertEquals("'" + orderA + "'.compareTo('" + orderB + "') customer comparison error.", -1,
					orderA.compareTo(orderB));

			// Different itemName
			orderB = new Order("OrdererA", "ItemNameB", 10);
			assertEquals("'" + orderA + "'.compareTo('" + orderB + "') itemName comparison error.", -1,
					orderA.compareTo(orderB));

			// Different amount
			orderB = new Order("OrdererA", "ItemNameA", 11);
			assertEquals("'" + orderA + "'.compareTo('" + orderB + "') amount comparison error.", -1,
					orderA.compareTo(orderB));

			// Second order smaller
			// Different customer
			orderB = new Order("Orderer0", "ItemNameA", 10);
			assertEquals("'" + orderA + "'.compareTo('" + orderB + "') customer comparison error.", 1,
					orderA.compareTo(orderB));

			// Different itemName
			orderB = new Order("OrdererA", "ItemName0", 10);
			assertEquals("'" + orderA + "'.compareTo('" + orderB + "') itemName comparison error.", 1,
					orderA.compareTo(orderB));

			// Different amount
			orderB = new Order("OrdererA", "ItemNameA", 9);
			assertEquals("'" + orderA + "'.compareTo('" + orderB + "') amount comparison error.", 1,
					orderA.compareTo(orderB));

			// Different everything
			orderB = new Order("OrdererC", "ItemNameC", 12);
			assertEquals("'" + orderA + "'.compareTo('" + orderB + "') unit comparison error.", -1,
					orderA.compareTo(orderB));

			logger.info("OK");
		} catch (Exception e) {
			TestUtils.handleErrorAndFail(e);
		}
	}
}
