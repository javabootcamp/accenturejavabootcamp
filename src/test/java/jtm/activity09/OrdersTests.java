package jtm.activity09;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ OrderDesignTest.class, OrdersDesignTest.class, OrderTest.class, OrdersTest.class })
public class OrdersTests {

}
