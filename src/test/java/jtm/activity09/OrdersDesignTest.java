package jtm.activity09;

import static jtm.TestUtils.checkMethods;
import static jtm.TestUtils.handleErrorAndFail;
import static jtm.TestUtils.toParagraph;
import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;

public class OrdersDesignTest {

	private static Logger logger = Logger.getLogger(OrdersDesignTest.class);

	@Test
	public final void test01OrderListDesign() {
		try {
			String[] list = { "public jtm.activity09.Orders()", "public boolean jtm.activity09.Orders.hasNext()",
					"public java.lang.Object jtm.activity09.Orders.next()",
					"public java.lang.String jtm.activity09.Orders.toString()",
					"public java.util.List jtm.activity09.Orders.getItemsList()",
					"public java.util.Set jtm.activity09.Orders.getItemsSet()",
					"public jtm.activity09.Order jtm.activity09.Orders.next()",
					"public void jtm.activity09.Orders.add(jtm.activity09.Order)",
					"public void jtm.activity09.Orders.remove()", "public void jtm.activity09.Orders.sort()" };
			assertEquals("Not all necessary methods are implemented for Orders. ", toParagraph(list),
					checkMethods(list, "jtm.activity09.Orders"));
		} catch (Exception e) {
			handleErrorAndFail(e, "Could not get list of methods for Orders");
		}
		logger.info("OK");
	}

}
