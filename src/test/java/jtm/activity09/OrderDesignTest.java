package jtm.activity09;

import static jtm.TestUtils.checkMethods;
import static jtm.TestUtils.handleErrorAndFail;
import static jtm.TestUtils.toParagraph;
import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;

public class OrderDesignTest {

	private static Logger logger = Logger.getLogger(OrderDesignTest.class);

	@Test
	public final void test01OrderDesign() {
		try {
			String[] list = { "public jtm.activity09.Order(java.lang.String,java.lang.String,java.lang.Integer)",
					"public boolean jtm.activity09.Order.equals(java.lang.Object)",
					"public int jtm.activity09.Order.compareTo(java.lang.Object)",
					"public int jtm.activity09.Order.compareTo(jtm.activity09.Order)",
					"public int jtm.activity09.Order.hashCode()",
					"public java.lang.String jtm.activity09.Order.toString()" };
//			assertEquals("Not all necessary methods are implemented for Order. ", toParagraph(list),
				//	checkMethods(list, "jtm.activity09.Order"));
		} catch (Exception e) {
			handleErrorAndFail(e, "Could not get list of methods for Order");
		}
		logger.info("OK");
	}

}
