
package jtm.activity06;

import static jtm.TestUtils.handleErrorAndFail;
import static jtm.TestUtils.randomInt;
import static org.junit.Assert.*;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MartianTest {

	private static Logger logger = Logger.getLogger(MartianTest.class);
	private Martian martian;
	private int testWeight = TestUtils.randomInt(10, 50);

	@Before
	public void setUp() throws Exception {
		martian = new Martian();
		martian.setWeight(testWeight);
	}

	@Test
	public void test1Porperties() {
		int testWeight = randomInt(10, 1);
		try {
			martian.setWeight(testWeight);
			martian.setWeight(testWeight * 2);
			assertEquals("Set weight error.", testWeight * 2, martian.getWeight());
			assertEquals("Arm count test error", 2, ((Humanoid) martian).getArmCount());
			assertEquals("Leg count test error", 7, ((Alien) martian).getLegCount());
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

	@Test
	public void test2Methods() {
		int alienWeight = randomInt(5, 1);
		Human human = new Human(alienWeight * 6);
		try {
			assertEquals("KillHimself error.", "I AM IMMORTAL!", martian.killHimself());
			martian.setWeight(alienWeight);
			martian.eatHuman(human);
			assertEquals("Eat alive human error.", alienWeight * 6 + alienWeight, martian.getWeight());
			assertEquals("Eat alive human error.", "Dead", human.isAlive());
			martian.eatHuman(human);
			assertEquals("Eat dead human error", alienWeight * 6 + alienWeight, martian.getWeight());
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

	@Test
	public void test3Backpack() {
		Martian martian = new Martian();

		int nonNullElements = 0;
		for(String str: martian.getBackpack()){
			if(str != null){
				nonNullElements++;
			}
		}
		assertEquals(0, nonNullElements);

		martian.addToBackpack("A");
		martian.addToBackpack("B");
		martian.addToBackpack("C");
		martian.addToBackpack("D");


		nonNullElements = 0;
		for(String str: martian.getBackpack()){
			if(str != null){
				nonNullElements++;
			}
		}
		assertEquals(4, nonNullElements);

		martian = new Martian();
		nonNullElements = 0;
		for(String str: martian.getBackpack()){
			if(str != null){
				nonNullElements++;
			}
		}
		assertEquals(0, nonNullElements);
		String[] arr = new String[10];
		for (int i = 0; i < 11; i++) {
			String word = TestUtils.randomLatinWord();
			if(i < 10){
				arr[i] = word;
			}
			martian.addToBackpack(word);
		}
		assertArrayEquals(arr, martian.getBackpack());

	}

	@Test
	public void test4Clone() throws Exception {
		int wMartian = TestUtils.randomInt(10, 10);
		int wHuman = TestUtils.randomInt(10, 10);

		Martian original = new Martian();
		original.setWeight(wMartian);

		Martian martian = (Martian)original.clone();
		assertTrue(martian != original);
		assertEquals(original.getWeight(), martian.getWeight());
		assertEquals(original.getArmCount(), martian.getArmCount());
		assertEquals(original.getLegCount(), martian.getLegCount());
		assertArrayEquals(original.getBackpack(), martian.getBackpack());

		Human human = new Human(wHuman);
		martian.eatHuman(human);

		Martian martian2 = (Martian)martian.clone();

		assertTrue(martian2 != original);
		assertEquals(martian.getWeight(), martian2.getWeight());
		assertEquals(martian.getArmCount(), martian2.getArmCount());
		assertEquals(martian.getLegCount(), martian2.getLegCount());
		assertArrayEquals(martian.getBackpack(), martian2.getBackpack());

	}
}
