package jtm.activity06;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ClassesTest.class, HumanTest.class, MartianTest.class })
public class InterfaceTests {

}
