package jtm.activity06;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.*;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HumanTest {
    private static Logger logger = Logger.getLogger(HumanTest.class);
    private Human human;
    private int testWeight = TestUtils.randomInt(10, 50);

    @Test
    public void test01Constructors() {
        int humanWeight = TestUtils.randomInt(10, 50);
        human = new Human();
        try {
            assertEquals("Wrong alive status.", "Alive", human.isAlive());
            assertEquals("Wrong weight.", 42, human.getWeight());
            human = new Human(humanWeight);
            assertEquals("Wrong alive status.", "Alive", human.isAlive());
            assertEquals("Wrong weight.", humanWeight, human.getWeight());
            logger.info("OK");
        } catch (Exception e) {
            handleErrorAndFail(e);
        }
    }

    @Test
    public void test02HumanWeight() {
        human = new Human(testWeight);
        try {
            assertEquals("Wrong alive status.", "Alive", human.isAlive());
            assertEquals("Wrong weight.", testWeight, human.getWeight());
            human.setWeight(22);
            assertEquals("Wrong weight.", 22, human.getWeight());
            logger.info("OK");
        } catch (Exception e) {
            handleErrorAndFail(e);
        }
    }

    @Test
    public void test03KillHimself() {
        human = new Human(TestUtils.randomInt(10, 50));
        try {
            assertEquals("Human test error.", "Alive", human.isAlive());
            assertEquals("Human KillHimself error", "Dead", human.killHimself());
            assertEquals("Human KillHimself error", "Dead", human.isAlive());
            logger.info("OK");
        } catch (Exception e) {
            handleErrorAndFail(e);
        }
    }

    @Test
    public void test04Arms() {
        human = new Human(TestUtils.randomInt(10, 50));
        try {
            assertEquals("Arm count test error", 2, human.getArmCount());
            logger.info("OK");
        } catch (Exception e) {
            handleErrorAndFail(e);
        }
    }

    @Test
    public void test05Backpack() {
        human = new Human(testWeight);

        int nonNullElements = 0;
        for(String str: human.getBackpack()){
        	if(str != null){
        		nonNullElements++;
			}
		}
        assertEquals(0, nonNullElements);

        human.addToBackpack("A");
        human.addToBackpack("B");
        human.addToBackpack("C");
        human.addToBackpack("D");


		nonNullElements = 0;
		for(String str: human.getBackpack()){
			if(str != null){
				nonNullElements++;
			}
		}
		assertEquals(4, nonNullElements);

        human = new Human(testWeight);
		nonNullElements = 0;
		for(String str: human.getBackpack()){
			if(str != null){
				nonNullElements++;
			}
		}
        assertEquals(0, nonNullElements);
        String[] arr = new String[10];
        for (int i = 0; i < 11; i++) {
			String word = TestUtils.randomLatinWord();
			if(i < 10){
				arr[i] = word;
			}
			human.addToBackpack(word);
        }
        assertArrayEquals(arr, human.getBackpack());

    }

}
