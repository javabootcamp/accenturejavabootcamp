package jtm.activity06;

import static jtm.TestUtils.checkMethods;
import static jtm.TestUtils.handleErrorAndFail;
import static jtm.TestUtils.toParagraph;
import static org.junit.Assert.assertEquals;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClassesTest {
    private static Logger logger = Logger.getLogger(ClassesTest.class);

    @Test
    public void test01CheckClasses() {
        try {
            Class.forName("jtm.activity06.Human");
        } catch (Exception e) {
            handleErrorAndFail(e, "Human class is not declared.");
        }
        try {
            Class.forName("jtm.activity06.Martian");
        } catch (Exception e) {
            handleErrorAndFail(e, "Martian class is not declared.");
        }
        logger.debug("OK");
    }

    @Test
    public void test02CheckDesignHuman() {

        try {
            String[] list = {
                    "public jtm.activity06.Human()",
                    "public jtm.activity06.Human(int)",
                    "public int jtm.activity06.Human.getArmCount()",
                    "public int jtm.activity06.Human.getWeight()",
                    "public java.lang.String jtm.activity06.Human.isAlive()",
                    "public java.lang.String jtm.activity06.Human.killHimself()",
                    "public void jtm.activity06.Human.addToBackpack(java.lang.String)",
                    "public void jtm.activity06.Human.setWeight(int)"
            };

            // Check, that all methods are implemented for tested classes
            assertEquals("Not all necessary methods are implemented for Human. ", toParagraph(list),
                    checkMethods(list, "jtm.activity06.Human"));
        } catch (Exception t) {
            handleErrorAndFail(t, "Could not get list of methods for Human");
        }
    }

    @Test
    public void test02CheckDesignMartian() {
        try {
            String[] list = {
                    "public int jtm.activity06.Martian.getArmCount()",
                    "public int jtm.activity06.Martian.getLegCount()",
                    "public int jtm.activity06.Martian.getWeight()",
                    "public java.lang.String jtm.activity06.Martian.isAlive()",
                    "public java.lang.String jtm.activity06.Martian.killHimself()",
                    "public java.lang.String[] jtm.activity06.Martian.getBackpack()",
                    "public void jtm.activity06.Martian.addToBackpack(java.lang.String)",
                    "public void jtm.activity06.Martian.eatHuman(jtm.activity06.Humanoid)",
                    "public void jtm.activity06.Martian.setWeight(int)"
            };
            assertEquals("Not all necessary methods are implemented for Martian. ", toParagraph(list),
                    checkMethods(list, "jtm.activity06.Martian"));
        } catch (Exception t) {
            handleErrorAndFail(t, "Could not get list of methods for Martian");
        }
    }
}