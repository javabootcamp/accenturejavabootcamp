
package jtm.extra02;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;

public class ArrayListMethodsTest {
	private static Logger logger = Logger.getLogger(ArrayListMethodsTest.class);

	@Test
	public final void testAll() {
		try {
			ArrayListMethods test = new ArrayListMethods();
			ArrayListMethods test2 = new ArrayListMethods();
			assertEquals("Array test error.", 2, test.checkArray(4, 2, 3, 6, 7, 8).size());
			assertEquals("Array test error.", 5, test.sumResult());
			assertEquals("Array test error.", 4, test2.checkArray(8, 2, 3, 6, 7, 8).size());
			assertEquals("Array test error.", 18, test2.sumResult());
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}
}
