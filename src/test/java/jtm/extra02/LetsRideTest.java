package jtm.extra02;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;

public class LetsRideTest {

	private static Logger logger = Logger.getLogger(LetsRideTest.class);

	@Test
	public final void testLetsRide() {
		try {
			LetsRide test = new LetsRide(4, 3, 20);
			assertEquals("Bust stop count error.", 4, test.getBusStopCount());
			assertEquals("Passengers at start error.", 3, test.getPassengersAtStart());
			assertEquals("Seat counts error.", 20, test.getSeatsCount());
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

	@Test
	public final void testPassengersAtRouteEnd() {
		try {
			LetsRide test = new LetsRide(5, 3, 25);
			assertEquals("Passengers at end error.", 18, test.passengersAtRouteEnd());

			test = new LetsRide(3, 1, 20);
			assertEquals("Passengers at end error.", 7, test.passengersAtRouteEnd());
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

	@Test
	public final void testFreeSeats() {
		try {
			LetsRide test = new LetsRide(5, 3, 25);
			test.setPassengersCount(7);
			assertEquals("Free seats error.", 18, test.freeSeats());

			test = new LetsRide(5, 3, 20);
			test.setPassengersCount(7);
			assertEquals("Free seats error.", 13, test.freeSeats());
			logger.info("OK");

		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

	@Test
	public final void testIsFull() {
		try {
			LetsRide test = new LetsRide(20, 4, 25);
			test.setPassengersCount(25);
			assertEquals("isFull() test error.", true, test.isFull());

			test = new LetsRide(20, 4, 30);
			test.setPassengersCount(10);
			assertEquals("isFull() test error.", false, test.isFull());
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

}
