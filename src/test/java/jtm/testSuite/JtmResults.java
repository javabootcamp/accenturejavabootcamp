package jtm.testSuite;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class JtmResults implements Comparable<JtmResults> {

	private static Logger logger = Logger.getLogger(JtmResults.class);
	static List<JtmResults> jtmResults;
	private static boolean printed; // FIXME workaround to hide several calls

	String resultEntry; // pack1.pack2.Class:method of description
	int totalTests;
	int passedTests;

	public JtmResults(String resultEntry, int testCount, int passedCount) {
		this.resultEntry = resultEntry;
		totalTests = testCount;
		passedTests = passedCount;
	}

	public static synchronized void addDescription(Description description) {
		if (jtmResults == null) {
			jtmResults = new LinkedList<JtmResults>();
			Collections.synchronizedCollection(jtmResults);
		}

		String classPath = description.getClassName();
		String methodName = description.getMethodName();
		int passed = 1;

		if (methodName == null) { // If called from JtmSuite runChild()
			methodName = ""; // create failed entry at beginning of package
			passed = 0; // results
		}
		classPath += ":" + methodName;
		logger.trace("addDescription: " + classPath);
		JtmResults tmp = new JtmResults(classPath, description.testCount(), passed);
		if (!jtmResults.contains(tmp))
			jtmResults.add(tmp);
	}

	public static synchronized void addFailure(Failure failure) {
		String className = failure.getDescription().getClassName() + ":" + failure.getDescription().getMethodName();
		logger.trace("addFailure: " + className);
		JtmResults tmp = new JtmResults(className, failure.getDescription().testCount(), 0);
		// Nullify passedTests for appropriate result entries
		int index = jtmResults.indexOf(tmp);
		if (index >= 0)
			jtmResults.set(index, tmp);
	}

	public static void printStatistics(Result resultTotal) {
		// FIXME should investigate why it is called several times from
		// JtmRunListener testRunFinished()
		if (!printed) {
			printed = true;
			Collections.sort(jtmResults);
			String prevpack = "", curpack = "", delim = "";
			StringBuilder tmp = new StringBuilder("\n*** Restult for packages ***\n");

			// Prepare list of packages
			for (JtmResults result : jtmResults) {
				curpack = getCurpack(result.resultEntry);
				if (!curpack.equals(prevpack) && !curpack.equals("testSuite"))
					tmp.append(delim + curpack);
				prevpack = curpack;
				delim = "\t";
			}
			tmp.append("\n");

			// Prepare list of test results for each package
			delim = "";
			prevpack = "";
			int totalTests = 0, passedTests = 0;
			for (JtmResults result : jtmResults) {
				curpack = getCurpack(result.resultEntry);

				// Add total results when new package is started
				if (!curpack.equals(prevpack) && !"".equals(prevpack)) {
					if (totalTests == 0)
						totalTests = 1;
					tmp.append(delim + "=" + passedTests + "/" + totalTests);
					totalTests = 0;
					passedTests = 0;
					delim = "\t";
				}

				if (!getMethod(result.resultEntry).equals(""))
					totalTests++;

				passedTests += result.passedTests;
				prevpack = curpack;

			}
			// No need to add last result as it is total result of testSuite
			// package

			int passed = resultTotal.getRunCount() - resultTotal.getFailureCount();
			if (passed < 0)
				passed = 0;
			tmp.append("\n*** Total restult ***\n=" + (passed + "/" + resultTotal.getRunCount()));

			logger.trace(jtmResults.toString());
			logger.info(tmp.toString());
		}

	}

	/**
	 * @param resultEntry
	 * @return method name of result entry, could be empty if entry was created
	 *         from test suite
	 */
	private static String getMethod(String resultEntry) {
		return resultEntry.substring(resultEntry.indexOf(':') + 1);
	}

	/**
	 * @param resultEntry
	 * @return name of last package from result entry
	 */
	private static String getCurpack(String resultEntry) {
		System.out.println(resultEntry);
		//return resultEntry.substring(resultEntry.indexOf('.') + 1, resultEntry.lastIndexOf('.'));
		return resultEntry;
	}

	/*
	 * Uniqueness is checked by resultEntry field
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof JtmResults) {
			JtmResults tmp = (JtmResults) obj;
			return this.resultEntry.equals(tmp.resultEntry);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return resultEntry.hashCode();
	}

	@Override
	public String toString() {
		return resultEntry + " = " + passedTests + "/" + totalTests;

	}

	@Override
	public int compareTo(JtmResults o) {
		return resultEntry.compareTo(o.resultEntry);
	}

}
