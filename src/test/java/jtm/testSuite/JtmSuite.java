package jtm.testSuite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.Suite;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.RunnerBuilder;

/**
 * This class is used to ensure listed unit test classes are executed in proper
 * order and results are collected with JtmRunListener according to the class
 * path name of the tests.
 */
public class JtmSuite extends Suite {
	static Logger logger = Logger.getLogger(JtmSuite.class);

	public JtmSuite(Class<?> klass, RunnerBuilder builder) throws InitializationError {
		super(klass, builder);
	}

	/*
	 * This method is overridden to ensure tests are sorted by classpath when
	 * executed
	 * 
	 * @see org.junit.runners.Suite#getChildren()
	 */
	@Override
	protected List<Runner> getChildren() {
		List<Runner> runners = super.getChildren();
		SortedMap<String, Runner> runnerMap = new TreeMap<String, Runner>();
		List<Runner> sortedRunners = new ArrayList<>();

		for (Runner runner : runners)
			runnerMap.put(runner.getDescription().getClassName(), runner);

		for (Entry<String, Runner> entry : runnerMap.entrySet())
			sortedRunners.add(entry.getValue());

		logger.trace("Sorted list of classes:" + runnerMap.keySet().toString());

		return Collections.unmodifiableList(sortedRunners);
	}

	/*
	 * This method is overridden to handle tests called from test suite
	 * 
	 * (Note that runChild() is not usable from test suite, as it is called only
	 * when entire suite is executed.)
	 * 
	 * @see org.junit.runners.Suite#runChild(org.junit.runner.Runner,
	 * org.junit.runner.notification.RunNotifier)
	 */
	@Override
	protected void runChild(Runner runner, RunNotifier notifier) {

		// Note that runner.getDescription().getMethodName() returns null from
		// here

		// Prepare listener
		notifier.addListener(new JtmRunListener());

		// Explicitly fire testRunStarted event, otherwise it is lost
		// (Currently this event is actually not handled.)
		notifier.fireTestRunStarted(getDescription());

		// Add entry to JtmResults from here to get statistics if test fail in
		// @BeforeClass section
		JtmResults.addDescription(runner.getDescription());

		// Do actual run
		super.runChild(runner, notifier);

	}

}