package jtm.testSuite;

import org.apache.log4j.Logger;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

/**
 * Customized RunListener which is called from JtmSuite
 */
public class JtmRunListener extends RunListener {
	private static Logger logger = Logger.getLogger(JtmRunListener.class);

	@Override
	public void testRunStarted(Description description) throws Exception {
		logger.trace("testRunStarted:" + description.getClassName() + ":" + description.getMethodName());
		JtmResults.addDescription(description);
		super.testRunStarted(description);
	}

	@Override
	public void testStarted(Description description) throws Exception {
		logger.trace("testStarted:" + description.getClassName() + ":" + description.getMethodName());
		JtmResults.addDescription(description);
		super.testStarted(description);
	}

	@Override
	public void testFinished(Description description) throws Exception {
		logger.trace("testFinished:" + description.getClassName() + ":" + description.getMethodName());
		super.testFinished(description);
	}

	@Override
	public void testRunFinished(Result result) throws Exception {
		logger.trace("testRunFinished: result=" + (result.getRunCount() - result.getFailureCount()) + "/"
				+ result.getRunCount());
		super.testRunFinished(result);
		for (Failure failure : result.getFailures())
			JtmResults.addFailure(failure);
		JtmResults.printStatistics(result);

	}
}