package jtm.activity10;

import jtm.TestUtils;
import org.junit.Test;

import static jtm.TestUtils.handleErrorAndFail;

public class PersonTest {

    @Test
    public void testClass() {
        try {
            Class.forName("jtm.activity10.Person");
        } catch (Exception e) {
            handleErrorAndFail(e, "SimpleCalcException class is not declared.");
        }
    }

}
