package jtm.activity10;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FileCopyTest {

    private FileCopy fileCopy;

    @Before
    public void setUp(){
         fileCopy = new FileCopy();
    }

    @Test
    public void fileCopyTest() throws Exception {
        Path path = Paths.get(getClass().getClassLoader().getResource("data.json").toURI());
        List<String> linez = Files.readAllLines(path);
        Files.write(Paths.get("/tmp/data.json"),linez);
        fileCopy.copyFile("/tmp/data.json");
        List<String> copy = Files.readAllLines(Paths.get("/tmp/data.json_backup"));
        assertEquals(linez.size(),copy.size());

    }
}
