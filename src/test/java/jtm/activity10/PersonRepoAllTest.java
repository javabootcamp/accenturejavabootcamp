package jtm.activity10;

import jtm.activity09.OrderDesignTest;
import jtm.activity09.OrderTest;
import jtm.activity09.OrdersDesignTest;
import jtm.activity09.OrdersTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({PersonTest.class, PersonRepoTest.class})
public class PersonRepoAllTest {
}


