package jtm.activity10;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PersonRepoTest {

    PersonRepo personRepo;

    @Before
    public void setUp() {
        personRepo = new PersonRepo();
    }


    @Test
    public void youngestTest() {
        Person person = personRepo.youngestPerson();
        System.out.println(person);
        assertEquals("Greenland", person.getCountry());
        assertEquals("Uummannaq", person.getCity());
        assertEquals("Fivechat", person.getCompany());
        assertEquals("Toby", person.getFirstName());
        assertEquals("Walbrun", person.getLastName());
    }

    @Test
    public void oldestTest() {
        Person person = personRepo.oldestPerson();
        System.out.println(person);
        assertEquals("Faroe Islands", person.getCountry());
        assertEquals("Argir", person.getCity());
        assertEquals("Topicstorm", person.getCompany());
        assertEquals("Xenos", person.getFirstName());
        assertEquals("Alwen", person.getLastName());
    }

    @Test
    public void largestPopulationTest() {
        String country = personRepo.largestPopulation();
        assertEquals("china", country.toLowerCase());
    }


}
