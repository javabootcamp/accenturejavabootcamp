package jtm.extra13;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConferenceTravellerTest {

    @Test
    public void noCitiesVisitedTest() {
        assertEquals("Stockholm", ConferenceTraveller.conferencePicker(
                new String[]{},
                new String[]{"Stockholm", "Paris", "Melbourne"}));
    }

    @Test
    public void noNewCitiesTest() {
        assertEquals("No worthwhile conferences this year!", ConferenceTraveller.conferencePicker(
                new String[]{"London", "Mexico City", "Melbourne", "Buenos Aires", "Berlin", "Hong Kong"},
                new String[]{"Berlin", "Melbourne"}));
    }

    @Test
    public void sampleTest() {
        assertEquals("Paris", ConferenceTraveller.conferencePicker(
                new String[]{"Mexico City","Johannesburg","Stockholm","Osaka","Saint Petersburg","London"},
                new String[]{"Stockholm","Paris","Melbourne"}));
    }



}
