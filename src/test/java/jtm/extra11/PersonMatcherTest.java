package jtm.extra11;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jtm.TestUtils;
import jtm.activity03.RandomPerson;
import org.apache.log4j.Logger;
 import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PersonMatcherTest {

	static Random rnd = new Random();
	static PersonMatcher manager;
	static final int minAge = 10;
	static final int minWeight = 20;
	static final int maxAge = 100;
	static final int maxWeight = 100;
	static int totalCount = TestUtils.randomInt(600, 200);
	static boolean isFemale = rnd.nextBoolean();
	static int ageFrom = TestUtils.randomInt(5, minAge);
	static int ageTo = TestUtils.randomInt(80, ageFrom);
	static int weightFrom = TestUtils.randomInt(10, minWeight);
	static int weightTo = TestUtils.randomInt(80, weightFrom);
	static List<RandomPerson> matchedPersons;

	static char[] smileys = { '☺', '☹', '☻' };

	private static Logger logger = Logger.getLogger(PersonMatcherTest.class);

	@Test
	public void test01Classes() {
		manager = PersonMatcher.getPersonManager();
	}

	@Test
	public void test02AddRandomPersons() {
		matchedPersons = new ArrayList<>();
		for (int i = 0; i < totalCount; i++) {
			ExtendedRandomPerson person = new ExtendedRandomPerson();
			int age = TestUtils.randomInt(maxAge - minAge, minAge);
			float weight = TestUtils.randomInt(maxWeight - minWeight, minWeight);
			boolean isFemale = rnd.nextBoolean();
			String name = TestUtils.randomName(isFemale);
			person.setName(name);
			person.setFemale(isFemale);
			person.setAge(age);
			person.setWeight(weight);
			person.setFemale(isFemale);
			person.setSmile(smileys[rnd.nextInt(3)]);
			if (isFemale == PersonMatcherTest.isFemale && age >= ageFrom && age <= ageTo && weight >= weightFrom
					&& weight <= weightTo) {
				matchedPersons.add(person);
			}
			manager.addPerson(person);
		}
	}

	@Test
	public void test03MatchingPersons() {
		Stream<RandomPerson> stream = manager.getPersonStream();
		assertEquals("Wrong count of RandomPersons in stream", totalCount, stream.count());
		assertEquals("Wrong count of RandomPersons in list", totalCount, manager.getPersonList().size());

		String matchedPersonsExpected = matchedPersons.stream().map(Object::toString).collect(Collectors.joining("\n"));
		Stream<RandomPerson> matchedPersonsActualStream = manager.getMatchedPersonStream(manager.getPersonStream(),
				isFemale, ageFrom, ageTo, weightFrom, weightTo);
		String matchedPersonsActual = matchedPersonsActualStream.map(Object::toString)
				.collect(Collectors.joining("\n"));
		assertEquals("Wrong list of matched persons", matchedPersonsExpected, matchedPersonsActual);

		logger.info("OK");

	}

	class ExtendedRandomPerson extends RandomPerson {
		@Override
		public String toString() {
			String isFemale;
			if (this.isFemale())
				isFemale = "F";
			else
				isFemale = "M";
			return isFemale + " " + getName() + " " + getAge() + " " + getWeight() + " " + getSmile();
		}
	}

}
