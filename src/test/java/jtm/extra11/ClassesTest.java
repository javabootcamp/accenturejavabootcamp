package jtm.extra11;

import static jtm.TestUtils.handleErrorAndFail;
import static jtm.TestUtils.toParagraph;
import static org.junit.Assert.assertEquals;

import jtm.TestUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClassesTest {

	@Test // Test that implementing class is created
	public void test01Classes() {
		try {
			Class.forName("jtm.extra11.PersonMatcherImpl");
		} catch (Exception e) {
			handleErrorAndFail(e, "PersonMatcherImpl class is not declared.");
		}
	}

	@Test // Test signatures of interface and implementing class
	public void test02Methods() {

		String[] methods = {

				"public abstract java.util.List jtm.extra11.PersonMatcher.getPersonList()",
				"public abstract java.util.stream.Stream jtm.extra11.PersonMatcher.getPersonStream()",
				"public abstract void jtm.extra11.PersonMatcher.addPerson(jtm.activity03.RandomPerson)",
				"public default java.util.stream.Stream jtm.extra11.PersonMatcher.getMatchedPersonStream(java.util.stream.Stream,boolean,int,int,float,float)",
				"public static java.util.List jtm.extra11.PersonMatcher.getPersonList(java.util.stream.Stream)",
				"public static jtm.extra11.PersonMatcher jtm.extra11.PersonMatcher.getPersonManager()"

		};

		assertEquals("Not all necessary methods are declared for PersonMatcher interface", toParagraph(methods),
				TestUtils.checkMethods(methods, "jtm.extra11.PersonMatcher"));

		String[] methods1 = {

				"public default java.util.stream.Stream jtm.extra11.PersonMatcher.getMatchedPersonStream(java.util.stream.Stream,boolean,int,int,float,float)",
				"public java.util.List jtm.extra11.PersonMatcherImpl.getPersonList()",
				"public java.util.stream.Stream jtm.extra11.PersonMatcherImpl.getPersonStream()",
				"public void jtm.extra11.PersonMatcherImpl.addPerson(jtm.activity03.RandomPerson)"

		};

		assertEquals("Not all necessary methods are declared for PersonMatcherImpl class", toParagraph(methods1),
				TestUtils.checkMethods(methods1, "jtm.extra11.PersonMatcherImpl"));

	}

}
