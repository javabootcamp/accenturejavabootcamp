package jtm.activity13.a;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest({HelloController.class})
public class HelloControllerTest {


    @Autowired
    private MockMvc mockMvc;


    @Test
    public void testHelloNoParams() throws Exception {

        mockMvc.perform(get("/hello"))
                .andExpect(status().isOk())
                .andExpect(result -> {
                    String resp = result.getResponse().getContentAsString();
                    assertEquals("Hello world!", resp);
                });
    }

    @Test
    public void testHelloParams() throws Exception {

        mockMvc.perform(get("/hello?name=John"))
                .andExpect(status().isOk())
                .andExpect(result -> {
                    String resp = result.getResponse().getContentAsString();
                    assertEquals("Hello John!", resp);
                });
    }

    @Test
    public void testNumbersNoParams() throws Exception {

        mockMvc.perform(get("/numbers"))
                .andExpect(status().isOk())
                .andExpect(result -> {
                    String resp = result.getResponse().getContentAsString();
                    assertEquals("12345678910111213141516171819", resp);
                });
    }

    @Test
    public void testNumbersParams() throws Exception {

        mockMvc.perform(get("/numbers?from=10&to=20"))
                .andExpect(status().isOk())
                .andExpect(result -> {
                    String resp = result.getResponse().getContentAsString();
                    assertEquals("10111213141516171819", resp);
                });
    }


}
