package jtm.activity14;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jtm.activity12.Teacher;
import jtm.activity13.b.StatusResponse;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.List;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = TeacherMain.class)
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class TeacherJPAControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @BeforeClass
    public static void setUp() {
        try {
            // Load the driver class.
            Class.forName("com.mysql.jdbc.Driver");
            //Create connection
            Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/db?autoReconnect=true&useSSL=false&characterEncoding=utf8", "admin", "abcd1234"
            );
            conn.setAutoCommit(false);
            conn.prepareStatement("SET SQL_SAFE_UPDATES = 0;").execute();
            PreparedStatement pStmt = conn
                    .prepareStatement("INSERT INTO Teacher (id, firstName, lastName) VALUES (?,?,?)");
            pStmt.setInt(1, 11111);
            pStmt.setString(2, "Rumpelstilzchen");
            pStmt.setString(3, "Angelica");
            pStmt.executeUpdate();
            pStmt = conn
                    .prepareStatement("INSERT INTO Teacher (id, firstName, lastName) VALUES (?,?,?)");
            pStmt.setInt(1, 11112);
            pStmt.setString(2, "Tybalt");
            pStmt.setString(3, "Rapunzel");
            pStmt.executeUpdate();
            pStmt = conn
                    .prepareStatement("INSERT INTO Teacher (id, firstName, lastName) VALUES (?,?,?)");
            pStmt.setInt(1, 11113);
            pStmt.setString(2, "Yvain");
            pStmt.setString(3, "Cressida");
            pStmt.executeUpdate();
            conn.commit();
            System.out.println("INSERTER");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void cleanUp() {
        try {

            Class.forName("com.mysql.jdbc.Driver");
            //Create connection
            Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/db?autoReconnect=true&useSSL=false&characterEncoding=utf8", "admin", "abcd1234"
            );
            conn.setAutoCommit(false);
            PreparedStatement pStmt = conn
                    .prepareStatement("DELETE FROM Teacher WHERE firstName in ('Frodo','Rumpelstilzchen','Tybalt','Yvain','Frodo','Faust')\n" +
                            "or lastName in ('Lalla','Angelica','Rapunzel','Cressida','Lalla','Gall');");
            pStmt.executeUpdate();
            conn.commit();
        } catch (Exception e) {

        }
    }


    @Test
    public void test01GetById() throws Exception {

        mockMvc.perform(get("/teacher/find/byId?id=11111"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(result -> {
                    String json = result.getResponse().getContentAsString();
                    ObjectMapper mapper = new ObjectMapper();
                    Teacher teacher = mapper.readValue(json, Teacher.class);
                    assertNotNull(teacher);
                    assertNotNull(teacher.getId());
                    assertNotNull(teacher.getFirstName());
                    assertNotNull(teacher.getLastName());
                    assertEquals(11111, teacher.getId());
                    assertEquals("Rumpelstilzchen".toLowerCase(), teacher.getFirstName().toLowerCase());
                    assertEquals("Angelica".toLowerCase(), teacher.getLastName().toLowerCase());
                });
    }

    @Test
    public void test02GetByName() throws Exception {
        mockMvc.perform(get("/teacher/find/byName?firstName=Tybalt&lastName=Rapunzel"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(result -> {
                    String json = result.getResponse().getContentAsString();
                    ObjectMapper mapper = new ObjectMapper();
                    List<Teacher> teacherz = mapper.readValue(json, new TypeReference<List<Teacher>>() {
                    });
                    assertNotNull(teacherz);
                    assertTrue(teacherz.size() > 0);
                    assertNotNull(teacherz.get(0).getId());
                    assertNotNull(teacherz.get(0).getFirstName());
                    assertNotNull(teacherz.get(0).getLastName());
                    assertEquals(11112, teacherz.get(0).getId());
                    assertEquals("Tybalt".toLowerCase(), teacherz.get(0).getFirstName().toLowerCase());
                    assertEquals("Rapunzel".toLowerCase(), teacherz.get(0).getLastName().toLowerCase());
                });
    }

    @Test
    public void test03AddNoId() throws Exception {
        mockMvc.perform(get("/teacher/add?firstName=Faust&lastName=Gall"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(result -> {
                    String json = result.getResponse().getContentAsString();
                    ObjectMapper mapper = new ObjectMapper();
                    StatusResponse response = mapper.readValue(json, StatusResponse.class);
                    assertNotNull(response);
                    assertTrue(response.getSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals("Success".toLowerCase(), response.getMessage().toLowerCase());
                });
    }



    @Test
    public void test04UpdateSucess() throws Exception {
        mockMvc.perform(get("/teacher/update?id=11111&firstName=Frodo&lastName=Lalla"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(result -> {
                    String json = result.getResponse().getContentAsString();
                    ObjectMapper mapper = new ObjectMapper();
                    StatusResponse response = mapper.readValue(json, StatusResponse.class);
                    assertNotNull(response);
                    assertTrue(response.getSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals("Success".toLowerCase(), response.getMessage().toLowerCase());
                });
    }

    @Test
    public void test05UpdateFailure() throws Exception {
        mockMvc.perform(get("/teacher/update?id=999999&firstName=Frodo&lastName=Lalla"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(result -> {
                    String json = result.getResponse().getContentAsString();
                    ObjectMapper mapper = new ObjectMapper();
                    StatusResponse response = mapper.readValue(json, StatusResponse.class);
                    assertNotNull(response);
                    assertFalse(response.getSuccess());
                    assertNotNull(response.getMessage());
                    assertEquals("Failure".toLowerCase(), response.getMessage().toLowerCase());
                });
    }
}
