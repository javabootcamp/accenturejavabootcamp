
package jtm.activity07;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

public class HawkTest {
	private static Logger logger = Logger.getLogger(HawkTest.class);
	private static Object bird, hawk;

	@BeforeClass
	public static void setUp() {
		bird = TestUtils.createObject("jtm.activity07.Bird");
		hawk = TestUtils.createObject("jtm.activity07.Hawk");
		TestUtils.checkExtension(hawk, bird);
	}

	@Test
	public void testProperties() {
		try {
			TestUtils tu = new TestUtils();
			logger.info(tu.listFields(hawk));
			fail("Hawk properies are not private");
		} catch (Exception e) {
			assertEquals("Hawk properties are not private",e.getClass(), IllegalAccessException.class);
		}
	}

	@Test
	public void testAge() {
		AnimalTest.testAge(hawk, "jtm.activity07.Bird");
	}

	@Test
	public void testCanFly() {
		BirdTest.testCanFly(hawk, "jtm.activity07.Hawk");
	}

	@Test
	public void testColor() {
		String name = TestUtils.randomWord();
		try {
			TestUtils.invokeSetMethod(hawk, "jtm.activity07.Hawk", "setColor", name);
			String res = (String) TestUtils.invokeGetMethod(hawk, "jtm.activity07.Dog", "getColor");
			assertEquals("Color test error.", name, res);
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

}
