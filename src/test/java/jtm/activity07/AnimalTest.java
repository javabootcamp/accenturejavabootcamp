package jtm.activity07;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.Random;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

public class AnimalTest {
	private static Logger logger = Logger.getLogger(AnimalTest.class);
	private static Animal animal;

	@BeforeClass
	public static void setUp() {
		TestUtils.checkOS();
		animal = (Animal) TestUtils.createObject("jtm.activity07.Animal");
	}

	@Test
	public void testAge() {
		testAge(animal, "jtm.activity07.Animal");
	}

	@Test
	public void testProperties() {
		try {
			TestUtils tu = new TestUtils();
			logger.info(tu.listFields(animal));
			fail("Animal properies are not private");

		} catch (Exception e) {
			assertEquals("Animal properties are not private",e.getClass(), IllegalAccessException.class);
			logger.info("OK");
		}
	}

	public static void testAge(Object obj, String classpath) {
		Random rnd = new Random();
		Integer age = new Integer(rnd.nextInt(10) + 1);
		try {
			TestUtils.invokeSetMethod(obj, classpath, "setAge", age);
			Integer res = (Integer) TestUtils.invokeGetMethod(obj, classpath, "getAge");
			assertEquals("Animal age get/set error", age, res);
			age = new Integer(rnd.nextInt(10) - 20);
			TestUtils.invokeSetMethod(obj, classpath, "setAge", age);
			res = (Integer) TestUtils.invokeGetMethod(obj, classpath, "getAge");
			assertEquals("Animal age cannot be set to negative value.", (Integer) 0, res);
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

}
