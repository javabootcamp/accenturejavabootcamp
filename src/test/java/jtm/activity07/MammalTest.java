package jtm.activity07;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Random;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

public class MammalTest {
	private static Logger logger = Logger.getLogger(MammalTest.class);
	private static Object animal, mammal;

	@BeforeClass
	public static void setUp() {
		animal = TestUtils.createObject("jtm.activity07.Animal");
		mammal = TestUtils.createObject("jtm.activity07.Mammal");
		TestUtils.checkExtension(mammal, animal);
	}

	@Test
	public void testProperties() {
		try {
			TestUtils tu = new TestUtils();
			logger.info(tu.listFields(mammal));
			fail("Mammal properies are not private");
		} catch (Exception e) {
			assertEquals("Mammal properties are not private",e.getClass(), IllegalAccessException.class);
		}
	}

	@Test
	public void testIsDomestic() {
		testIsDomestic(mammal, "jtm.activity07.Mammal");
	}

	@Test
	public void testAge() {
		AnimalTest.testAge(mammal, "jtm.activity07.Mammal");
	}

	public static void testIsDomestic(Object obj, String classpath) {
		Random rnd = new Random();
		Boolean isDomestic = rnd.nextBoolean();
		try {
			TestUtils.invokeSetMethod(obj, classpath, "setIsDomestic", isDomestic);
			Boolean res = (Boolean) TestUtils.invokeGetMethod(obj, classpath, "getIsDomestic");
			assertEquals("isDomestic test error.", isDomestic, res);
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

}
