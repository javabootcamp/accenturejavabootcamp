package jtm.activity07;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Random;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

public class BirdTest {
	private static Logger logger = Logger.getLogger(BirdTest.class);
	private static Object animal, bird;

	@BeforeClass
	public static void setUp() {
		animal = TestUtils.createObject("jtm.activity07.Animal");
		bird = TestUtils.createObject("jtm.activity07.Bird");
		TestUtils.checkExtension(bird, animal);
	}

	@Test
	public void testProperties() {
		try {
			TestUtils tu = new TestUtils();
			logger.info(tu.listFields(bird));
			fail("Bird properies are not private");
		} catch (Exception e) {
			assertEquals("Bird properies are not private",e.getClass(), IllegalAccessException.class);
		}
	}

	@Test
	public void testCanFly() {
		testCanFly(bird, "jtm.activity07.Bird");
	}

	@Test
	public void testAge() {
		AnimalTest.testAge(bird, "jtm.activity07.Bird");
	}

	public static void testCanFly(Object obj, String classpath) {
		Random rnd = new Random();
		Boolean can = rnd.nextBoolean();
		try {
			TestUtils.invokeSetMethod(obj, classpath, "setCanFly", can);
			Boolean res = (Boolean) TestUtils.invokeGetMethod(obj, classpath, "getCanFly");
			assertEquals("canFly testing error.", can, res);
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

}
