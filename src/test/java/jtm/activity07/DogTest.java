package jtm.activity07;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

public class DogTest {
	private static Logger logger = Logger.getLogger(DogTest.class);
	private static Object mammal, dog;

	@BeforeClass
	public static void setUp() {
		mammal = TestUtils.createObject("jtm.activity07.Mammal");
		dog = TestUtils.createObject("jtm.activity07.Dog");
		TestUtils.checkExtension(dog, mammal);
	}

	@Test
	public void testProperties() {
		try {
			TestUtils tu = new TestUtils();
			logger.info(tu.listFields(dog));
			fail("Dog properies are not private");
		} catch (Exception e) {
			assertEquals("Dog properties are not private",e.getClass(), IllegalAccessException.class);
		}
	}

	@Test
	public void testAge() {
		AnimalTest.testAge(dog, "jtm.activity07.Dog");
	}

	@Test
	public void testIsDomestic() {
		MammalTest.testIsDomestic(dog, "jtm.activity07.Dog");
	}

	@Test
	public void testName() {

		String name, res;
		try {
			// Negative tests
			name = Integer.toString(TestUtils.randomInt(10, 1));
			TestUtils.invokeSetMethod(dog, "jtm.activity07.Dog", "setName", name);
			res = (String) TestUtils.invokeGetMethod(dog, "jtm.activity07.Dog", "getName");
			assertEquals("Dog's name cannot contain digits.", "", res);

			name = TestUtils.randomLatinWord();
			TestUtils.invokeSetMethod(dog, "jtm.activity07.Dog", "setName", name);
			res = (String) TestUtils.invokeGetMethod(dog, "jtm.activity07.Dog", "getName");
			assertEquals("Dog's name should start with capital letter", "", res);

			name = TestUtils.randomLatinName();
			TestUtils.invokeSetMethod(dog, "jtm.activity07.Dog", "setName", name);
			res = (String) TestUtils.invokeGetMethod(dog, "jtm.activity07.Dog", "getName");
			assertEquals("Dog's name set/get error", name, res);

			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

}
