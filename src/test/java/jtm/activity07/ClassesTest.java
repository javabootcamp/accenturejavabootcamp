package jtm.activity07;

import static jtm.TestUtils.checkMethods;
import static jtm.TestUtils.handleErrorAndFail;
import static jtm.TestUtils.toParagraph;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ClassesTest {

	@Test
	public void test01Classes() {
		try {
			Class.forName("jtm.activity07.Animal");
		} catch (Exception e) {
			handleErrorAndFail(e, "Animal class is not declared.");
		}
		try {
			Class.forName("jtm.activity07.Mammal");
		} catch (Exception e) {
			handleErrorAndFail(e, "Mammal class is not declared.");
		}
		try {
			Class.forName("jtm.activity07.Dog");
		} catch (Exception e) {
			handleErrorAndFail(e, "Dog class is not declared.");
		}
		try {
			Class.forName("jtm.activity07.Bird");
		} catch (Exception e) {
			handleErrorAndFail(e, "Bird class is not declared.");
		}
		try {
			Class.forName("jtm.activity07.Hawk");
		} catch (Exception e) {
			handleErrorAndFail(e, "Hawk class is not declared.");
		}
	}

	@Test
	public void test02Methods() {
		try {
			String[] methods = { "public jtm.activity07.Animal()", "public int jtm.activity07.Animal.getAge()",
					"public void jtm.activity07.Animal.setAge(int)" };
			assertEquals("Not all necessary methods are implemented for Animal. ", toParagraph(methods),
					checkMethods(methods, "jtm.activity07.Animal"));
		} catch (Exception e) {
			handleErrorAndFail(e, "Could not get list of methods for Animal");
		}
		try {
			String[] methods = { "public jtm.activity07.Mammal()",
					"public boolean jtm.activity07.Mammal.getIsDomestic()", "public int jtm.activity07.Animal.getAge()",
					"public void jtm.activity07.Animal.setAge(int)",
					"public void jtm.activity07.Mammal.setIsDomestic(boolean)" };
			assertEquals("Not all necessary methods are implemented for Mammal. ", toParagraph(methods),
					checkMethods(methods, "jtm.activity07.Mammal"));
		} catch (Exception e) {
			handleErrorAndFail(e, "Could not get list of methods for Mammal");
		}
		try {
			String[] methods = { "public jtm.activity07.Dog()", "public boolean jtm.activity07.Mammal.getIsDomestic()",
					"public int jtm.activity07.Animal.getAge()", "public java.lang.String jtm.activity07.Dog.getName()",
					"public void jtm.activity07.Animal.setAge(int)",
					"public void jtm.activity07.Dog.setName(java.lang.String)",
					"public void jtm.activity07.Mammal.setIsDomestic(boolean)" };
			assertEquals("Not all necessary methods are implemented for Dog. ", toParagraph(methods),
					checkMethods(methods, "jtm.activity07.Dog"));
		} catch (Exception e) {
			handleErrorAndFail(e, "Could not get list of methods for Dog");
		}
		try {
			String[] methods = { "public jtm.activity07.Bird()", "public boolean jtm.activity07.Bird.getCanFly()",
					"public int jtm.activity07.Animal.getAge()", "public void jtm.activity07.Animal.setAge(int)",
					"public void jtm.activity07.Bird.setCanFly(boolean)" };
			assertEquals("Not all necessary methods are implemented for Bird. ", toParagraph(methods),
					checkMethods(methods, "jtm.activity07.Bird"));
		} catch (Exception e) {
			handleErrorAndFail(e, "Could not get list of methods for Bird");
		}
		try {
			String[] methods = { "public jtm.activity07.Hawk()", "public boolean jtm.activity07.Bird.getCanFly()",
					"public int jtm.activity07.Animal.getAge()",
					"public java.lang.String jtm.activity07.Hawk.getColor()",
					"public void jtm.activity07.Animal.setAge(int)",
					"public void jtm.activity07.Bird.setCanFly(boolean)",
					"public void jtm.activity07.Hawk.setColor(java.lang.String)" };
			assertEquals("Not all necessary methods are implemented for Hawk. ", toParagraph(methods),
					checkMethods(methods, "jtm.activity07.Hawk"));
		} catch (Exception e) {
			handleErrorAndFail(e, "Could not get list of methods for Hawk");
		}
	}

}
