package jtm;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

/**
 * Utility class to show dialog message should be called by main() method
 */
public class DialogWindow extends JDialog implements ActionListener {

	private static final long serialVersionUID = -7104512129440836354L;
	private JButton okBtn;
	private static JLabel messageLbl;

	public DialogWindow() {
		super(new JFrame(), "", Dialog.ModalityType.APPLICATION_MODAL);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setUndecorated(true);
		this.setLayout(new FlowLayout(FlowLayout.CENTER));
		okBtn = new JButton("OK");
		add(messageLbl);
		okBtn.addActionListener(this);
		add(okBtn);
		setLocationRelativeTo(null);
		getRootPane().setDefaultButton(okBtn);
		requestFocusInWindow();
		getRootPane().setBorder(BorderFactory.createMatteBorder(1, 1, 4, 4, Color.DARK_GRAY));
		pack();

		// Close dialog after timeout
		Timer timer = new Timer(3000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});
		timer.setRepeats(false);
		timer.start();

		// Show dialog
		setVisible(true);
	}

	public DialogWindow(String message) {
		this();
		messageLbl = new JLabel(message);
	}

	/**
	 * This is entry point to the class
	 * 
	 * @param args
	 *            — message which should be shown into dialog window
	 */
	public static void main(String[] args) {
		String message = "Please pass argument \"message\"";
		if (args != null && args.length != 0) {
			message = args[0];
		}
		messageLbl = new JLabel(message);
		new DialogWindow();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.dispose();
	}

}