package jtm.tasks.fundamentals;

import jtm.tasks.fundamentals.EvenNumbers;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

/*
Find numbers which are divisible by given number
https://www.codewars.com/kata/55edaba99da3a9c84000003b/train/java
 */
public class EvenNumbersTest {

    @Test
    public void testSimple() {
        assertTrue(Arrays.equals(new int[]{2, 4, 6}, EvenNumbers.divisibleBy(new int[]{1, 2, 3, 4, 5, 6}, 2)));
        assertTrue(Arrays.equals(new int[]{3, 6}, EvenNumbers.divisibleBy(new int[]{1, 2, 3, 4, 5, 6}, 3)));
        assertTrue(Arrays.equals(new int[]{0, 4}, EvenNumbers.divisibleBy(new int[]{0, 1, 2, 3, 4, 5, 6}, 4)));
    }
}
