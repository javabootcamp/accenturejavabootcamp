package jtm.tasks.fundamentals;

import jtm.tasks.fundamentals.PhoneNumber;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/*
    Create Phone Number
    https://www.codewars.com/kata/525f50e3b73515a6db000b83/train/java
 */
public class PhoneNumberTest {

    @Test
    public void tests() {
        assertEquals("(123) 456-7890", PhoneNumber.createPhoneNumber(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}));
    }
}
