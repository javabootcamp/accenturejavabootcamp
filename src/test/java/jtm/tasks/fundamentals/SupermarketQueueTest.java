package jtm.tasks.fundamentals;

import jtm.tasks.fundamentals.SupermarketQueue;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/*
    The Supermarket Queue
    https://www.codewars.com/kata/57b06f90e298a7b53d000a86/train/java
 */
public class SupermarketQueueTest {

    @Test
    public void testSamples() {
        assertEquals(12, SupermarketQueue.solveSuperMarketQueue(new int[]{5, 3, 4}, 1));
        assertEquals(10, SupermarketQueue.solveSuperMarketQueue(new int[]{10, 2, 3, 3}, 2));
        assertEquals(12, SupermarketQueue.solveSuperMarketQueue(new int[]{2, 3, 10}, 2));
    }

    @Test
    public void testNormalCondition() {
        assertEquals(9, SupermarketQueue.solveSuperMarketQueue(new int[]{2, 2, 3, 3, 4, 4}, 2));
    }

    @Test
    public void testEmptyArray() {
        assertEquals(0, SupermarketQueue.solveSuperMarketQueue(new int[]{}, 1));
    }

    @Test
    public void testSingleTillManyCustomers() {
        assertEquals(15, SupermarketQueue.solveSuperMarketQueue(new int[]{1, 2, 3, 4, 5}, 1));
    }
}
