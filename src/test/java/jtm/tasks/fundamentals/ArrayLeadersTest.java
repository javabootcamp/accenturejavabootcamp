package jtm.tasks.fundamentals;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ArrayLeadersTest {

    @Test
    public void Check_Positive_Values() {
        assertArrayEquals(new int[]{4}, ArrayLeaders.arrayLeaders(new int[]{1, 2, 3, 4, 0}));
        assertArrayEquals(new int[]{17, 5, 2}, ArrayLeaders.arrayLeaders(new int[]{16, 17, 4, 3, 5, 2}));
    }

    @Test
    public void Check_Negative_Values() {
        assertArrayEquals(new int[]{-1}, ArrayLeaders.arrayLeaders(new int[]{-1, -29, -26, -2}));
        assertArrayEquals(new int[]{-36, -12}, ArrayLeaders.arrayLeaders(new int[]{-36, -12, -27}));
    }

    @Test
    public void Mixed_Values() {
        assertArrayEquals(new int[]{5, 2}, ArrayLeaders.arrayLeaders(new int[]{5, 2}));
        assertArrayEquals(new int[]{0, -1, 3, 2}, ArrayLeaders.arrayLeaders(new int[]{0, -1, -29, 3, 2}));
    }

}
