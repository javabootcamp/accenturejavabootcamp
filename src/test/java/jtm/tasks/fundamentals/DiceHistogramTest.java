package jtm.tasks.fundamentals;

import jtm.tasks.fundamentals.DiceHistogram;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/*
Histogram - H1
https://www.codewars.com/kata/57d532d2164a67cded0001c7/train/java
 */
public class DiceHistogramTest {

    @Test
    public void basic() {
        final String expected =
                "6|##### 5\n" +
                        "5|\n" +
                        "4|# 1\n" +
                        "3|########## 10\n" +
                        "2|### 3\n" +
                        "1|####### 7\n";
        assertEquals(expected, DiceHistogram.histogram(new int[]{7, 3, 10, 1, 0, 5}));
    }
}
