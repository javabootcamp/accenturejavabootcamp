package jtm.extra01;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import jtm.TestUtils;
import org.apache.log4j.Logger;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class GetOneTest {

	private static Logger logger = Logger.getLogger(GetOneTest.class);
	private GetOne test = new GetOne();
	
	@Rule
	public Timeout globalTimeout = Timeout.seconds(5);

	@Test
	public final void testIterations() {
		try {
			assertEquals("Wrong iteration count", 0, test.iterations(1));
			assertEquals("Wrong iteration count", 1, test.iterations(2));
			assertEquals("Wrong iteration count", 16, test.iterations(7));
			assertEquals("Wrong iteration count", 143, test.iterations(327));
			assertNotEquals("Wrong iteration count", 0, test.iterations(TestUtils.randomInt(100, 200)));
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

	@Test
	public final void testMostComplexNo() {
		try {
			assertEquals("Wrong the most complicated number", 1, test.theMostComplexNo(1));
			assertEquals("Wrong the most complicated number", 2, test.theMostComplexNo(2));
			assertEquals("Wrong the most complicated number", 3, test.theMostComplexNo(3));
			assertEquals("Wrong the most complicated number", 3, test.theMostComplexNo(4));
			assertEquals("Wrong the most complicated number", 6, test.theMostComplexNo(6));
			assertEquals("Wrong the most complicated number", 171, test.theMostComplexNo(200));
			assertEquals("Wrong the most complicated number", 327, test.theMostComplexNo(351));
			assertNotEquals("Wrong the most complicated number", 1,
					test.theMostComplexNo(TestUtils.randomInt(100, 100)));
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}
}
