package jtm.extra01;

import static jtm.TestUtils.handleErrorAndFail;
import static jtm.extra01.Zodiac.getZodiac;
import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;

public class ZodiacTest {
	private static Logger logger = Logger.getLogger(ZodiacTest.class);

	@Test
	public void testGetZodiac() {
		try {

			assertEquals("AQUARIUS", getZodiac(25, 1).toUpperCase());
			assertEquals("TAURUS", getZodiac(21, 4).toUpperCase());
			assertEquals("TAURUS", getZodiac(21, 5).toUpperCase());
			assertEquals("VIRGO", getZodiac(23, 9).toUpperCase());
			assertEquals("LEO", getZodiac(22, 8).toUpperCase());
			assertEquals("ARIES", getZodiac(21, 3).toUpperCase());
			assertEquals("PISCES", getZodiac(19, 3).toUpperCase());
			assertEquals("ARIES", getZodiac(20, 4).toUpperCase());
			assertEquals("GEMINI", getZodiac(22, 5).toUpperCase());
			assertEquals("GEMINI", getZodiac(21, 6).toUpperCase());
			assertEquals("CANCER", getZodiac(22, 6).toUpperCase());
			assertEquals("CANCER", getZodiac(22, 7).toUpperCase());
			assertEquals("LEO", getZodiac(23, 7).toUpperCase());
			assertEquals("VIRGO", getZodiac(23, 8).toUpperCase());
			assertEquals("VIRGO", getZodiac(22, 9).toUpperCase());
			assertEquals("LIBRA", getZodiac(22, 10).toUpperCase());
			assertEquals("LIBRA", getZodiac(23, 10).toUpperCase());
			assertEquals("SCORPIO", getZodiac(22, 11).toUpperCase());
			assertEquals("SAGITTARIUS", getZodiac(23, 11).toUpperCase());
			assertEquals("SAGITTARIUS", getZodiac(21, 12).toUpperCase());
			assertEquals("CAPRICORN", getZodiac(22, 12).toUpperCase());
			assertEquals("CAPRICORN", getZodiac(19, 1).toUpperCase());
			assertEquals("CAPRICORN", getZodiac(20, 1).toUpperCase());
			assertEquals("AQUARIUS", getZodiac(21, 1).toUpperCase());
			assertEquals("AQUARIUS", getZodiac(19, 2).toUpperCase());
			assertEquals("PISCES", getZodiac(20, 2).toUpperCase());
			assertEquals("PISCES", getZodiac(21, 2).toUpperCase());
			assertEquals("PISCES", getZodiac(20, 3).toUpperCase());

			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}

	}

}
