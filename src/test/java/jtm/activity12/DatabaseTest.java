
package jtm.activity12;

import org.apache.log4j.Logger;
import org.junit.*;
import org.junit.rules.Timeout;
import org.junit.runners.MethodSorters;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.List;

import static jtm.TestUtils.*;
import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DatabaseTest {

    @Rule
    public Timeout globalTimeout = Timeout.seconds(5);

    // Common trigger to reset database only once per test suite
    private static Logger logger = Logger.getLogger(DatabaseTest.class);

    private static Connection conn;
    private static TeacherManager manager;

    @AfterClass
    public static void cleanUp() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        //Create connection
        conn = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/db?autoReconnect=true&useSSL=false&characterEncoding=utf8", "admin", "abcd1234"
        );
        conn.setAutoCommit(false);
        PreparedStatement pStmt = conn
                .prepareStatement("DELETE FROM Teacher WHERE id > ? and id < ?");
        pStmt.setInt(1, 9000);
        pStmt.setInt(2, 10100);
        pStmt.executeUpdate();
        conn.commit();
    }

    @BeforeClass
    public static void setUp() {
        manager = new TeacherManager();
        try {
            // Load the driver class.
            Class.forName("com.mysql.jdbc.Driver");
            //Create connection
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/db?autoReconnect=true&useSSL=false&characterEncoding=utf8", "admin", "abcd1234"
            );
            PreparedStatement pStmt = conn
                    .prepareStatement("INSERT INTO Teacher (id, firstName, lastName) VALUES (?,?,?)");
            pStmt.setInt(1, 10001);
            pStmt.setString(2, "NvasfhscnfDSGXSD");
            pStmt.setString(3, "DoeMnvvadiucnbsad");
            pStmt.executeUpdate();
            pStmt = conn
                    .prepareStatement("INSERT INTO Teacher (id, firstName, lastName) VALUES (?,?,?)");
            pStmt.setInt(1, 10002);
            pStmt.setString(2, "Janesfnahjkpcmsjc");
            pStmt.setString(3, "DoeMnvvadiucnbsad");
            pStmt.executeUpdate();
            pStmt = conn
                    .prepareStatement("INSERT INTO Teacher (id, firstName, lastName) VALUES (?,?,?)");
            pStmt.setInt(1, 10003);
            pStmt.setString(2, "Cnutsafnasbufihop");
            pStmt.setString(3, "DoeMnvvadiucnbsad");
            pStmt.executeUpdate();
            conn.commit();

        } catch (Exception e) {

        }
    }


    @Test
    public void test01FindTeacherByID() {
        try {
            Teacher result = manager.findTeacher(10001);
            assertEquals("NvasfhscnfDSGXSD", result.getFirstName());
            assertEquals("DoeMnvvadiucnbsad", result.getLastName());
            // Negative test
            assertNull(manager.findTeacher(-128));
            logger.info("OK");
        } catch (Exception e) {
            handleErrorAndFail(e);
        }
    }

    @Test
    public void test02FindTeachersByNames() {
        try {
            // Positive tests
            List<Teacher> results = manager.findTeacher("", "DoeMnvvadiucnbsad");
            assertTrue("findTeacher by empty name() error.",
                    1 <= results.size());

            results = manager.findTeacher("NvasfhscnfDSGXSD", "Csygfbfisafasfbafsbasf");
            assertEquals("findTeacher() result size error.", 1, results.size());
            // Negative tests
            results = manager.findTeacher("tt7t76t6t76t76std76ats", "should NOT be found");
            assertEquals("findTeacher() error for negative search.", 0, results.size());
            logger.info("OK");
        } catch (Exception e) {
            handleErrorAndFail(e);
        }
    }

    @Test
    public void test03InsertTeacher() {
        try {
            // Only positive test
            String fname = "testF";
            String lname = "testL";
            boolean result = manager.insertTeacher(fname, lname);
            assertTrue(result);
            List<Teacher> results = manager.findTeacher(fname, lname);
            assertEquals("findTeacher() result size error.", 1, results.size());
            checkTeachers(results, fname, lname);
            logger.info("OK");
        } catch (Exception e) {
            handleErrorAndFail(e);
        } finally {

        }

    }

    @Test
    public void test04InsertTeacherObjcet() {
        try {
            // Positive test
            String fname = "nsufshfafsf122";
            String lname = "sfafafsfsf122";
            int id = 10007;
            Teacher teacher = new Teacher(id, fname, lname);
            boolean result = manager.insertTeacher(teacher);
            assertTrue(result);
            // Search for inserted
            List<Teacher> results = manager.findTeacher(fname, lname);
            assertEquals("insertTeacherAll() results size error.", 1, results.size());
            // Negative tests
            // Insert again the same values
            result = manager.insertTeacher(teacher);
            assertFalse(result);
            // Search again
            results = manager.findTeacher(fname, lname);
            assertEquals("insertTeacherAll() results size error.", 1, results.size());
        } catch (Exception e) {
            handleErrorAndFail(e);
        }

    }

    @Test
    public void test05UpdateTeacher() {
        try {
            String name = "fsahd1yehdsgbd";
            String last = "fsahd1yehdsgbd";
            Teacher teacher = new Teacher(10003, name, last);
            boolean result = manager.updateTeacher(teacher);
            assertTrue(result);

            List<Teacher> results = manager.findTeacher(name, last);
            assertEquals("updateTeacher() results size error.", 1, results.size());
            checkTeachers(results, name, last, (long) 10003, 0);

            // Negative test with non existing teacher
            String fname = "This teacher";
            String lname = "Should not be updated";
            teacher = new Teacher(10015, fname, lname);
            result = manager.updateTeacher(teacher);
            assertFalse(result);
            results = manager.findTeacher(fname, lname);
            assertEquals("updateTeacher() results size error.", 0, results.size());

            logger.info("OK");
        } catch (Exception e) {
            handleErrorAndFail(e);
        }

    }

    @Test
    public void test06DeleteTeacher() {
        try {
            // Delete
            boolean result = manager.deleteTeacher(10003);
            assertTrue(result);
            // Check
            List<Teacher> results = manager.findTeacher("Cnutsafnasbufihop", "DoeMnvvadiucnbsad");
            for (Teacher teacher : results) {
                assertNotEquals(10003, teacher.getId());
            }
            // Delete again
            result = manager.deleteTeacher(10003);
            assertFalse(result);
            logger.info("OK");
        } catch (Exception e) {
            handleErrorAndFail(e);
        }

    }

    @Test
    public void test07InjectionTests() {
        try {
            boolean result = manager.insertTeacher("A','A');" + "drop database database_activity;"
                    + "insert into database_activity.Teacher (firstname, lastname) VALUES ('B", "B");
            assertFalse("Negative test with SQL injection passed", result);
            logger.info("OK");
        } catch (Exception e) {
            handleErrorAndFail(e);
        }

    }

    @Test
    public void test10closeConnection() {
        try {
            if (manager != null)
                manager.closeConnection();
            assertNull("Connection is not set to null when closed.", manager.conn);
        } catch (Exception e) {
            handleErrorAndFail(e);
        }
    }


    private void checkTeachers(List<Teacher> results, String firstname, String lastname) {
        checkTeachers(results, firstname, lastname, null, null, null);
    }

    private void checkTeachers(List<Teacher> results, String firstname, String lastname, Long id, Integer recordno) {
        checkTeachers(results, firstname, lastname, id, recordno, null);
    }

    private void checkTeachers(List<Teacher> results, String firstname, String lastname, Long id, Integer recordno,
                               String additionalMessage) {
        if (recordno == null)
            recordno = 0;
        if (additionalMessage == null)
            additionalMessage = "";
        else
            additionalMessage = " " + additionalMessage;
        assertNotNull("List of teachers is null. If no teachers are found, empty list should be returned.", results);
        assertEquals("Firstname comparison error." + additionalMessage, firstname,
                results.get(recordno).getFirstName());
        assertEquals("Lastname comparison error." + additionalMessage, lastname, results.get(recordno).getLastName());
        if (id != null)
            assertEquals("ID comparison error.", (long) id, results.get(recordno).getId());
    }



}
