package jtm.activity14;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TeacherMain {

    private static Logger logger = LoggerFactory.getLogger(TeacherMain.class);

    public static void main(String[] args) {
        SpringApplication.run(TeacherMain.class);
    }

    @Bean
    /*
        TODO
        This code will be automatically executed after application starts up
        You can use this to test repository or other parts of the task

        Use logger.info(<SOME STRING>) to print information into console in Spring Applications
        e.g. logger.info("Test");
        See how logger object is created above
     */
    public CommandLineRunner demo(TeacherRepository repository) {
        return (args) -> {
            logger.info(repository.findById(900).toString());
        };
    }
}
