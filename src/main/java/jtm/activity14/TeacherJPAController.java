package jtm.activity14;

import jtm.activity13.b.StatusResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TeacherJPAController {

    /*
        TODO
        Add @Autowired annotation to repository field below.
        Spring will automatically create TeacherRepository object and inject object into manager field
     */
    @Autowired
    private TeacherRepository repository;

    @GetMapping("/teacher/find/byId")
    public TeacherEntity findTeacher(@RequestParam Integer id) {
        /* TODO
           Return Teacher object by Id from Teacher table in database  */
        return repository.findById(id).get();
    }

    /*
        TODO set get mapping path to /teacher/add
     */
    @GetMapping("/teacher/add")
    public StatusResponse insertTeacher(@RequestParam String firstName,
                                        @RequestParam String lastName) {
        /*
            TODO
            Insert new Teacher object into database, if ID parameter is not passed to request
            ID should be auto-generated
            Return StatusResponse object. If teacher is inserted set success to true,
            and message to "Success" otherwise false and "Failure"

         */
        TeacherEntity teacherEntity = repository.save(new TeacherEntity(null, firstName, lastName));
        if (teacherEntity != null) {
            return new StatusResponse(true, "Success");
        } else {
            return new StatusResponse(false, "Failure");
        }
    }


    /*
        TODO set get mapping path to /teacher/find/byName
     */
    @GetMapping("/teacher/find/byName")
    public List<TeacherEntity> findTeacher(@RequestParam String firstName,
                                           @RequestParam String lastName) {
        /* TODO
           Return list of Teacher objects by name and last name from Teacher table in database  */
        return repository.findAllByFirstNameOrLastName(firstName, lastName);
    }


    /*
        TODO set get mapping path to /teacher/update
     */
    @GetMapping("/teacher/update")
    public StatusResponse updateTeacher(@RequestParam Integer id,
                                        @RequestParam String firstName,
                                        @RequestParam String lastName) {

        /*
            TODO
            Update teacher in database by ID
            Return StatusResponse object. If teacher is updated set success to true,
            and message to "Success" otherwise false and "Failure"
         */
        TeacherEntity teacher = new TeacherEntity(id, firstName, lastName);
        if (repository.existsById(teacher.getId())) {
            repository.save(teacher);
            return new StatusResponse(true, "Success");
        } else {
            return new StatusResponse(false, "Failure");
        }

    }

}
