package jtm.activity14;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherRepository extends CrudRepository<TeacherEntity,Integer> {

    List<TeacherEntity> findAllByFirstNameOrLastName(String firstName, String lastName);
}
