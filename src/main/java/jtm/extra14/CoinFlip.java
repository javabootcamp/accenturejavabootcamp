package jtm.extra14;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class CoinFlip {

    /*
    You will be given an integer n, which is the number of times that is thown a coin.
    Return an array of string for all the possibilities (heads[H] and tails[T]). Examples:

    coin(1) should return {"H", "T"}
    coin(2) should return {"HH", "HT", "TH", "TT"}
    coin(3) should return {"HHH", "HHT", "HTH", "HTT", "THH", "THT", "TTH", "TTT"}

    When finished sort them alphabetically.
     */

    public static String[] coinFlip(int n) {
        // Do staff
        int totalCombos = (int) Math.pow(2, n);
        int length = n;

        List<String> combos = new ArrayList<>();

        String HEAD = "H";
        String TAILS = "T";

        combos.add(HEAD);
        combos.add(TAILS);
        for(int i=1; i < n; i++ ){
            List<String> tmp = new ArrayList<>();
            for(String combo: combos){
                tmp.add(combo+HEAD);
                tmp.add(combo+TAILS);
            }
            combos = tmp;
        }

        Collections.sort(combos);
        System.out.println(combos);
        return combos.toArray(new String[0]);
    }

    public static void main(String[] args) {
        //Expected "HH", "HT", "TH", "TT"
        String[] res = CoinFlip.coinFlip(2);
        System.out.println(res);
    }
}
