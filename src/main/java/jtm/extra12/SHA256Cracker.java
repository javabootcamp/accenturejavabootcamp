package jtm.extra12;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SHA256Cracker {


    /*
        TODO Read about SHA-256 algorithm and multiple ways how to hash strings in Java with SHA-256

        When provided with a SHA-256 hash, return the value that was hashed.
        You are also given the characters that make the expected value, but in alphabetical order.

        The returned value is less than 10 characters long.
        Return null when the hash cannot be cracked with the given characters.
     */

    static List<String> combos = new ArrayList<>();

    static String crackSha256(String hash, String chars) {
        permute("", chars);
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            for (String combo : combos) {
                byte[] encodedhash = digest.digest(combo.getBytes(StandardCharsets.UTF_8));
                String hexHash = bytesToHex(encodedhash);
                if (hexHash.equals(hash))
                    return combo;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    static void permute(String prefix, String s) {
        int N = s.length();

        if (N == 0) {
            combos.add(prefix);
        }

        for (int i = 0; i < N; i++)
            permute(prefix + s.charAt(i), s.substring(0, i) + s.substring(i + 1, N));
    }

    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public static void main(String[] args) {

        assertEquals("code", crackSha256("5694d08a2e53ffcae0c3103e5ad6f6076abd960eb1f8a56577040bc1028f702b", "cdeo"));
        assertEquals("GoOutside", crackSha256("b8c49d81cb795985c007d78379e98613a4dfc824381be472238dbd2f974e37ae", "deGioOstu"));
        assertEquals(null, crackSha256("f58262c8005bb64b8f99ec6083faf050c502d099d9929ae37ffed2fe1bb954fb", "abc"));
    }
}
