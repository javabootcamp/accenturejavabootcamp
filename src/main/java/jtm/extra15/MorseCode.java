package jtm.extra15;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class MorseCode {

    static Map<String, String> letters = new HashMap<>();

    static {
        letters.put("a", ".-");
        letters.put("b", "-...");
        letters.put("c", "-.-.");
        letters.put("d", "-..");
        letters.put("e", ".");
        letters.put("f", "..-.");
        letters.put("g", "--.");
        letters.put("h", "....");
        letters.put("i", "..");
        letters.put("j", ".---");
        letters.put("k", "-.-");
        letters.put("l", ".-..");
        letters.put("m", "--");
        letters.put("n", "-.");
        letters.put("o", "---");
        letters.put("p", ".--.");
        letters.put("q", "--.-");
        letters.put("r", ".-.");
        letters.put("s", "...");
        letters.put("t", "-");
        letters.put("u", "..-");
        letters.put("v", "...-");
        letters.put("w", ".--");
        letters.put("x", "-..-");
        letters.put("y", "-.--");
        letters.put("z", "--..");
        letters.put("1", ".----");
        letters.put("2", "..---");
        letters.put("3", "...--");
        letters.put("4", "....-");
        letters.put("5", ".....");
        letters.put("6", "-....");
        letters.put("7", "--...");
        letters.put("8", "---..");
        letters.put("9", "----.");
        letters.put("0", ".---- -----");
        letters.put(" ", "|");
    }


    /*
        TODO

        Implement function that translates given text to morse code
        Morse code encodings can be found in MorseLetters.txt
        Add one empty space after each morse letter so that it would be
            easier to see where one ends and the next one starts
        Replace empty spaces with straight line symbol: |

        Example:
            Thank you very much
            - .... .- -. -.- | -.-- --- ..- | ...- . .-. -.-- | -- ..- -.-. ....

            Note that morse code does not have capitalization e.g. T and t are both the same in morse code: '-'

     */
    public static String translateToMorse(String text) {
        StringBuilder sb = new StringBuilder();
        for (char c : text.toCharArray()) {
            String toAppend = letters.getOrDefault((c + "").toLowerCase(), c + "");
            sb.append(toAppend + " ");
        }
        return sb.toString().trim();
    }

    public static void main(String[] args) {
        String expected = "- .... .- -. -.- | -.-- --- ..- | ...- . .-. -.-- | -- ..- -.-. ....";
        String result = translateToMorse("Thank you very much");

        System.out.println("'" + expected + "'");
        System.out.println("'" + result + "'");
    }
}
