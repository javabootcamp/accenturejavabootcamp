package jtm.activity05;

import jtm.activity04.Road;

public class WaterRoad extends Road {

	public WaterRoad(String from, String to, int dist) {
		super(from, to, dist);
	}

	public WaterRoad() {
		super();

	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " " + super.toString();
	}

}
