package jtm.activity05;

import jtm.activity04.Road;

public class Amphibia extends Ship {
	private byte sails;
	private Vehicle vehicle;

	public Amphibia(String id, float consumption, int tankSize, byte sails, int wheels){
		super(id, sails);
		this.vehicle = new Vehicle(id,consumption,tankSize,wheels);
	}

	@Override
	public String move(Road road) {
		String status = "";
		if (road instanceof WaterRoad) {
			WaterRoad waterRoad = (WaterRoad) road;
			return getType() + " is sailing on " + waterRoad + " with " + sails + " sails";
		}
		if (road instanceof Road) {
			Road groundRoad = road;
			return super.move(groundRoad);
		}
		return status;
	}

}
