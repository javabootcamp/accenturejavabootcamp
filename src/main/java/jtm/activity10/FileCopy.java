package jtm.activity10;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileCopy {

    public static void main(String[] args){
        FileCopy fc = new FileCopy();
        fc.copyFile("studentsproject/src/main/resources/data.json");
    }

    public void copyFile(String filePath){
        System.out.println("Copying " + filePath);
        List<String> content = read(filePath);
        write(filePath+"_backup", content);

    }

    public void writeFile(String filePath, String content){
        File file = new File(filePath); // Create file object
        try {
            // Create buffered reader to read from input
            BufferedReader in = new BufferedReader(new StringReader(content));
            // Create PrintWriter to write to file
            PrintWriter out = new PrintWriter(new FileWriter(file));
            String s;
            while ((s = in.readLine()) != null) { // Read by line
                out.println(s);
            }
            in.close();   // Close reader from input
            out.close();  // Close writer to file
        } catch (IOException e) {
            e.printStackTrace(); // handle exceptions
        }
    }

    public String readFile(String filePath){
        File file = new File(filePath);
        //Reading from:
        System.out.println(file.getAbsolutePath());
        try {
            // create buffered reader
            BufferedReader in = new BufferedReader(new FileReader(file));

            String s = in.readLine(); // read line
            StringBuffer stringBuffer = new StringBuffer();
            while ( s != null ) { // print on console
                stringBuffer.append(s);
                s = in.readLine();
            }
            in.close(); // close reader
            return stringBuffer.toString();
        } catch (IOException e) {
            e.printStackTrace(); // handle exceptions
        }
        return null;
    }

    public void write(String path, List<String> linez){
        try {
            Files.write(Paths.get(path),linez);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> read(String path){
        try {
            return Files.readAllLines(Paths.get(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
