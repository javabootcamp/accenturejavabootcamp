package jtm.activity10;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class PersonRepo {

    List<Person> personList;

    public PersonRepo() {
        init();
    }

    public void init() {

        try {
            Path path = Paths.get(getClass().getClassLoader().getResource("data.json").toURI());
            byte[] bytes = Files.readAllBytes(path);
            String data = new String(bytes);
            ObjectMapper mapper = new ObjectMapper();
            Person[] personArray = mapper.readValue(data, Person[].class);
            personList = Arrays.asList(personArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Person oldestPerson() {
        personList.sort(new Comparator<Person>() {
            @Override
            public int compare(Person person, Person t1) {
                return person.getBirthDate().compareTo(t1.getBirthDate());
            }
        });
        return personList.get(0);
    }

    public Person youngestPerson() {
        personList.sort(new Comparator<Person>() {
            @Override
            public int compare(Person person, Person t1) {
                return t1.getBirthDate().compareTo(person.getBirthDate());
            }
        });
        return personList.get(0);
    }

    public String largestPopulation() {
        Map<String, Integer> population = new HashMap<>();
        for (Person p : personList) {
            if (population.containsKey(p.getCountry())) {
                int count = population.get(p.getCountry());
                count++;
                population.put(p.getCountry(), count);
            } else {
                population.put(p.getCountry(), 1);
            }
        }

        String country = "";
        Integer count = 0;
        for (String ctry : population.keySet()) {
            int cnt = population.get(ctry);
            if (cnt > count) {
                country = ctry;
                count = cnt;
            }
        }
        return country;

    }


}
