package jtm.activity04;

public class TrafficManagementSystem {
	static Transport[] transports;
	static Road[] roads;

	/**
	 * This method is called to set up TransportManagementSystem
	 * 
	 * @param roads
	 * @param transport
	 */
	public static void initSystem(int roads, int transport) {
		addRoads(roads);
		addTransport(transport);
	}

	public static Transport[] getTransports() {
		// TODO return required value
		// START
		return transports;
		// END
		// RMCOMM return null;
	}

	public static void addTransport(int i) {
		// TODO create new array of transports in size of passed value
		// START
		transports = new Transport[i];
		// END
	}

	public static void setVehicle(Transport transport, int i) {
		// TODO set passed transport into transports array cell of passed index
		// START
		transports[i] = transport;
		// END
	}

	public static void addRoads(int i) {
		// TODO create new array of roads in size of passed value
		// START
		roads = new Road[i];
		// END
	}

	public static Road[] getRoads() {
		// TODO return required value
		// START
		return roads;
		// END
		// RMCOMM return null;
	}

	public static void setRoad(Road road, int i) {
		// TODO set passed road into passed cell of roads array
		// START
		roads[i] = road;
		// END
	}

}
