package jtm.extra11;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jtm.activity03.RandomPerson;

public interface PersonMatcher {

	void addPerson(RandomPerson person);

	List<RandomPerson> getPersonList();

	// START
	Stream<RandomPerson> getPersonStream();

	default Stream<RandomPerson> getMatchedPersonStream(Stream<RandomPerson> persons, boolean isFemale, int ageFrom,
			int ageTo, float weightFrom, float weightTo) {
		return (Stream<RandomPerson>) persons.filter(p -> p.isFemale() == isFemale && p.getAge() >= ageFrom
				&& p.getAge() <= ageTo && p.getWeight() >= weightFrom && p.getWeight() <= weightTo);
	}

	static List<RandomPerson> getPersonList(Stream<RandomPerson> persons) {
		return persons.collect(Collectors.toList());
	}

	static PersonMatcher getPersonManager() {
		return new PersonMatcherImpl();
	}
	// END

}
