package jtm.extra11;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import jtm.activity03.RandomPerson;

// This class is deleted from exercise
public class PersonMatcherImpl implements PersonMatcher {
	static List<RandomPerson> persons;

	public PersonMatcherImpl() {
		persons = new ArrayList<>();
	}

	@Override
	public void addPerson(RandomPerson person) {
		persons.add(person);
	}

	@Override
	public List<RandomPerson> getPersonList() {
		return persons;
	}

	@Override
	public Stream<RandomPerson> getPersonStream() {

		Stream.Builder<RandomPerson> builder = Stream.builder();
		for (RandomPerson person : persons) {
			builder.accept(person);
		}
		return builder.build();

	}
}
