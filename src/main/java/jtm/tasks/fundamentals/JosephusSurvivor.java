package jtm.tasks.fundamentals;

import java.util.Arrays;

public class JosephusSurvivor {

    /*
    TODO

    You have to correctly return who is the "survivor", ie: the last element of a Josephus permutation.

    Basically you have to assume that n people are put into a circle and that they are eliminated in steps of
    k elements, like this:

    josephus_survivor(7,3) => means 7 people in a circle; one every 3 is eliminated until one remains
    [1,2,3,4,5,6,7] - initial sequence
    [1,2,4,5,6,7] => 3 is counted out
    [1,2,4,5,7] => 6 is counted out
    [1,4,5,7] => 2 is counted out
    [1,4,5] => 7 is counted out
    [1,4] => 5 is counted out
    [4] => 1 counted out, 4 is the last element - the survivor!
    The above link about the "base" kata description will give you a more thorough insight about the origin of this
    kind of permutation, but basically that's all that there is to know to solve this.

    You may assume that both n and k will always be >=1.
     */

    public static int josephusSurvivor(final int n, final int k) {

        //Create starting array and fill with numbers
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = i + 1;
        }

        int index = -1; //variable to store current index from starting point in a circle
        int toAdd = k;  //count to add each time
        int[] tmpArr = Arrays.copyOf(array, array.length); //array to store result after each killing
        for (int i = 0; i < array.length - 1; i++) {
            int[] tempo = new int[tmpArr.length - 1]; //array to store result after this killing

            index = index + toAdd;  //add count to index
            while (index >= tmpArr.length) { //if we are out of bounds minus length will get us to right index
                index = index  - tmpArr.length;
            }

            //copy array up to the killed one (excluding)
            int[] firstPart = Arrays.copyOfRange(tmpArr, 0, index);
            //System.out.println("First" + Arrays.toString(firstPart));
            //copy array from the killed one (excluding) to end
            int[] secondPart = Arrays.copyOfRange(tmpArr, index + 1, tmpArr.length);
            //System.out.println("Second " + Arrays.toString(secondPart));

            //Combine two array parts into one
            System.arraycopy(firstPart, 0, tempo, 0, firstPart.length);
            System.arraycopy(secondPart, 0, tempo, firstPart.length, secondPart.length);
            //System.out.println("Full " + Arrays.toString(tempo));

            //Decrease index by one since we killed one of
            index--;
            //Replace array reference
            tmpArr = tempo;
        }

        return tmpArr[0];
    }

    public static void main(String args[]) {
        josephusSurvivor(7, 3);
    }
}
