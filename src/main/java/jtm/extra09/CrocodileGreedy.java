package jtm.extra09;

public class CrocodileGreedy implements Crocodile {

	private MoveStrategy moveStrategy;
	private int moves;
	private int candies;

	@Override
	public void move(Board board) {
		moveStrategy.move(this, board);
	}

	public void setMoves(int moves) {
		this.moves = moves;
	}

	public void setCandies(int candies) {
		this.candies = candies;
	}

	@Override
	public int getMoves() {
		return moves;
	}

	@Override
	public int getCandies() {
		return candies;
	}

	public CrocodileGreedy() {
		moves = 0;
		candies = 0;
		moveStrategy = new MoveGreedy();
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + ", candies: " + candies + ", moves:" + moves;
	}

	@Override
	public String getType() {
		return this.getClass().getSimpleName();
	}

}
