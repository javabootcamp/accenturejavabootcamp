package jtm.extra09;

import java.util.List;

/**
 * Crocodile board game which compares scores of different crocodiles
 * 
 */
public class CrocodileGame {
	public static Board board; // Shared board of the game
	public static List<Crocodile> crocodile; // List of crocodiles

	public static String runGame() {
		// TODO #1: Reset game board before each crocodile movement by using
		// .getClone()
		// method of the Board
		// TODO #2: Go through the list of Crocodiles and move them on the Board
		// using
		// .move() method of crocodile
		// TODO #3: Store score of the crocodile as ratio of eaten candies /
		// made moves
		// Calculate winning crocodile in following way:
		// 1. if score between previous and current crocodile is < 0.001, return
		// "Tie"
		// 2. if score of current crocodile is better return its "getType wins",
		// e.g. "CrocodileGreedy wins"

		// START
		String result = "";
		float cscore = 0f, prevscore = 0f;
		for (Crocodile c : crocodile) {
			c.move(board.getClone());
			cscore = (float) c.getCandies() / (float) c.getMoves();
			System.out.println(c.getType() + ": " + cscore);
			if (Float.toString(cscore).equals("Infinity") && Float.toString(prevscore).equals("Infinity")
					|| Float.toString(cscore).equals("NaN") && Float.toString(prevscore).equals("NaN")
					|| Math.abs(cscore - prevscore) < 0.001)
				result = "Tie";
			else if (cscore > prevscore)
				result = c.getType() + " wins";
			prevscore = cscore;
		}
		return result;
		// END
		// RMCOMM return "";
	}

}
