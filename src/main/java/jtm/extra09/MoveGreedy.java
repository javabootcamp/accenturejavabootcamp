package jtm.extra09;

/*-
 * Implement movement strategy in such way that crocodile covers all cells of
 * the board and eats all candies. Movement always starts and ends on X axis
 * e.g.
 * ⇾⇾⇾
 * ⇽⇽⇽
 * ⇾⇾⇾
 * 
 * If board has even number of rows, greedy crocodile walks on the last row twice:
 * ⇾⇾⇾
 * ⇽⇽⇽
 * ⇾⇾⇾
 * ⇆⇆⇆
 */
public class MoveGreedy implements MoveStrategy {

	/*
	 * @see jtm.activity17.MoveStrategy#move(Crocodile, Board)
	 */
	@Override
	public void move(Crocodile crocodile, Board board) {
		int x = 0, y = 0, i = 0, j = 0, candies = 0;
		int moves;
		x = board.getX();
		y = board.getY();
		CrocodileGreedy greedy = (CrocodileGreedy) crocodile;
		for (i = 0; i < x; i++) {
			for (j = 0; j < y; j++) {
				if (board.getCandy(i, j) == '●')
					candies++;
				board.setCandy(i, j, '◎');
			}
		}
		greedy.setCandies(candies);
		// If even number of rows, need to walk last row twice
		if (y % 2 == 0)
			moves = x * y - 1 + x - 1;
		// else just walk all rows
		else
			moves = x * y - 1;
		greedy.setMoves(moves);
	}

}
