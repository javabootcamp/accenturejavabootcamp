package jtm.extra09;

/*-
 * 
 * Implement movement strategy in such way, that crocodile covers only first row
 * and last column of the board. e.g.
 *  ⇾⇾⇾
 *    ↓
 *    ↓
 */
public class MoveSimple implements MoveStrategy {

	@Override
	public void move(Crocodile crocodile, Board board) {
		int x = 0, y = 0, i = 0, j = 0, candies = 0;
		CrocodileSimple simple = (CrocodileSimple) crocodile;
		x = board.getX();
		y = board.getY();
		for (i = 0; i < x; i++) {
			if (board.getCandy(i, j) == '●')
				candies++;
			board.setCandy(i, j, '◎');
		}
		// x is incremented one more time when for loop exits, therefore
		i--;
		for (j = 0; j < y; j++) {
			if (board.getCandy(i, j) == '●')
				candies++;
			board.setCandy(i, j, '◎');
		}
		simple.setCandies(candies);
		simple.setMoves(x + y - 2);
	}
}
