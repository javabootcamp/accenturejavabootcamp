package jtm.extra09;

public class CrocodileSimple implements Crocodile {
	private int moves;
	private int candies;
	private MoveStrategy moveStrategy;

	public CrocodileSimple() {
		moves = 0;
		candies = 0;
		moveStrategy = new MoveSimple();
	}

	@Override
	public void move(Board board) {
		moveStrategy.move(this, board);
	}

	@Override
	public int getMoves() {
		return moves;
	}

	@Override
	public int getCandies() {
		return candies;
	}

	public void setMoves(int moves) {
		this.moves = moves;
	}

	public void setCandies(int candies) {
		this.candies = candies;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + ", candies: " + candies + ", moves:" + moves;
	}

	@Override
	public String getType() {
		return this.getClass().getSimpleName();
	}

}
