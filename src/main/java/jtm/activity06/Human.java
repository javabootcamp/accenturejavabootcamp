package jtm.activity06;

/*
 * !!! Note that this class is not included in Student's workspace !!!
 */

public class Human implements Humanoid {
    private int weight;
    private boolean isAlive;
    private String[] backpack;

    public Human(int weight) {
        this();
        this.weight = weight;
    }

    public Human() {
        this.weight = 42;
        this.isAlive = true;
        this.backpack = new String[10];
    }

    @Override
    public String killHimself() {
        isAlive = false;
        return isAlive();
    }

    @Override
    public String isAlive() {
        if (isAlive) {
            return "Alive";
        } else {
            return "Dead";
        }
    }

    @Override
    public int getWeight() {
        return this.weight;
    }

    @Override
    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public int getArmCount() {
        return ARM_COUNT;
    }

    @Override
    public String[] getBackpack() {
        return backpack;
    }

    @Override
    public void addToBackpack(String item) {
        for (int i = 0; i < backpack.length; i++) {
            if (backpack[i] == null) {
                backpack[i] = item;
                break;
            }
        }
    }


    @Override
    public String toString() {
        return "Human: " + weight + " [" + backpack + "]";
    }
}
