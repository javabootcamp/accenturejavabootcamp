package jtm.activity06;

/*
 * !!! Note that this class is not included in Student's workspace !!!
 */

public class Martian implements Humanoid, Alien, Cloneable {

    private int weight;
    private String[] backpack;

    public Martian() {
        this.weight = 42;
        this.backpack = new String[10];
    }

    @Override
    public void eatHuman(Humanoid humanoid) {
        if (humanoid.isAlive().equals("Alive")) {
            humanoid.killHimself();
            this.weight += humanoid.getWeight();
        }
    }

    @Override
    public int getLegCount() {
        return LEG_COUNT;
    }

    @Override
    public int getWeight() {
        return this.weight;
    }

    @Override
    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String killHimself() {
        return this.isAlive();
    }

    @Override
    public int getArmCount() {
        return ARM_COUNT;
    }

    @Override
    public String[] getBackpack() {
        return backpack;
    }

    @Override
    public void addToBackpack(String item) {
        for (int i = 0; i < backpack.length; i++) {
            if (backpack[i] == null) {
                backpack[i] = item;
                break;
            }
        }
    }

    @Override
    public String isAlive() {
        return "I AM IMMORTAL!";
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Martian martian = new Martian();
        martian.setWeight(this.getWeight());
        for (int i = 0; i < this.backpack.length; i++) {
            if (backpack[i] != null) {
                martian.addToBackpack(backpack[i]);
            }
        }
        return martian;
    }
}
