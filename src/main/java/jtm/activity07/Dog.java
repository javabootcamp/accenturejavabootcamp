package jtm.activity07;

public class Dog extends Mammal {
	private String name;

	public void setName(String name) {
		// TODO set name for the Dog
		// START
		if (name.matches("[A-Z][a-z]*"))
			this.name = name;
		else
			this.name = "";
		// END
	}

	public String getName() {
		// TODO return name of the Dog
		// START
		return name;
		// END
		// RMCOMM return "";
	}

}
