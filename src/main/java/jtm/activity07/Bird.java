package jtm.activity07;

public class Bird extends Animal {

	private boolean canFly;

	public void setCanFly(boolean canFly) {
		// TODO set setCanFly property for the Bird
		// START
		this.canFly = canFly;
		// END
	}

	public boolean getCanFly() {
		// TODO return setCanFly property of the Bird
		// START
		return canFly;
		// END
		// RMCOMM return false;
	}

}
