package jtm.activity07;

public class Mammal extends Animal {
	private boolean isDomestic;

	public void setIsDomestic(boolean isDomestic) {
		// TODO set setIsDomestic property for the Mammal
		// START
		this.isDomestic = isDomestic;
		// END
	}

	public boolean getIsDomestic() {
		// TODO return setIsDomestic property of the Mammal
		// START
		return isDomestic;
		// END
		// RMCOMM return false;
	}
}
