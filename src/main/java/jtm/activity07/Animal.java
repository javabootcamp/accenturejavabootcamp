package jtm.activity07;

public class Animal {
	private int age;

	public void setAge(int age) {
		// TODO set age of the Animal
		// START
		if (age > 0)
			this.age = age;
		else
			this.age = 0;
		// END
	}

	public int getAge() {
		// TODO return age of the Animal
		// START
		return age;
		// END
		// RMCOM return 0;
	}

}
