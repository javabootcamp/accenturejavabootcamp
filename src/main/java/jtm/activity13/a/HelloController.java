package jtm.activity13.a;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {


    @GetMapping("/hello")
    public String hello(@RequestParam(required = false, defaultValue = "world") String name) {
        /* TODO
           Make Request Parameter name not required, and set defaultValue to "world"
           Return string value in form Hello <name>!, where <name> is value from name parameter */
        return String.format("Hello %s!", name);
    }

    @GetMapping("/numbers")
    public String numbers(@RequestParam(required = false, defaultValue = "1") Integer from,
                          @RequestParam(required = false, defaultValue = "20") Integer to) {
        /* TODO
           Make Request Parameter name not required, and set defaultValue to "world"
           Return string value in form Hello <name>!, where <name> is value from name parameter */
        StringBuilder sb = new StringBuilder();
        for (int i=from; i < to; i++){
            sb.append(i);
        }
        return sb.toString();
    }



}
