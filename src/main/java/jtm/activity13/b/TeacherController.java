package jtm.activity13.b;

import jtm.activity12.Teacher;
import jtm.activity12.TeacherManager;
import jtm.activity13.b.StatusResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TeacherController {

    @Autowired
    private TeacherManager manager;

    @GetMapping("/teacher/find/byId")
    public Teacher findTeacher(@RequestParam Integer id) {
        return manager.findTeacher(id);
    }

    @GetMapping("/teacher/find/byName")
    public List<Teacher> findTeacher(@RequestParam String firstName,
                                     @RequestParam String lastName) {
        return manager.findTeacher(firstName, lastName);
    }

    @GetMapping("/teacher/add")
    public StatusResponse insertTeacher(@RequestParam(required = false) Integer id,
                                        @RequestParam String firstName,
                                        @RequestParam String lastName) {
        boolean success;
        if (id == null) {
            success = manager.insertTeacher(firstName, lastName);
        } else {
            success = manager.insertTeacher(new Teacher(id, firstName, lastName));
        }
        return new StatusResponse(success ? true : false, success ? "Success" : "Failure");
    }

    @GetMapping("/teacher/update")
    public StatusResponse updateTeacher(@RequestParam Integer id,
                                        @RequestParam String firstName,
                                        @RequestParam String lastName) {
        boolean success =
                manager.updateTeacher(new Teacher(id, firstName, lastName));
        return new StatusResponse(success ? true : false, success ? "Success" : "Failure");
    }

}
